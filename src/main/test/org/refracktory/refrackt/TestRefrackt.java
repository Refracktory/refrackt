package org.refracktory.refrackt;

import com.knunk.resttest.util.RestTester;
import com.knunk.tools.resttool.RestTool;
import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
import org.refracktory.refrackt.services.techno.selectable.SelectableTokenizer;
import org.refracktory.refrackt.services.techno.tags.Tags;
import org.springframework.web.util.UriComponentsBuilder;
import unirest.HttpResponse;
import unirest.JsonNode;
import unirest.UnirestException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TestRefrackt {
    public static void main(String[] args) throws UnirestException, IOException {
        RestTester tester = new RestTester();

//        tester.testHttpStatus(testRefEvaluateGet("color"), 200);
//        tester.testHttpStatus(testRefEvaluateGet("xyzzy"), 404);
//
//        tester.test(testRefEvaluatePost("texture"), 200);
//        tester.test(testRefEvaluatePost("xyzzy"), 404);
//
//        tester.testHttpStatus(testRefTemplateGet("test"), 200);
//        tester.testHttpStatus(testRefTemplateGet("xyzzy"), 404);
//
//        tester.test(testGenerate("sheet", "lawful,good"), 200);
//        testTags();
//        HttpResponse<JsonNode> response = testGenerateViaPost("6x6grid", "lawful,good,pick-species,basic");

//      @RequestMapping(value = "/evaluate/{selector}/{tags}", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
//        HttpResponse<String> response = testEvaluateViaGet("henchman-type", "business,individual,cult");
        HttpResponse<JsonNode> response = testGenerateViaPost("cultures", "lawful,good,pick-species,basic");
        tester.testHttpStatus(response, 200);
        System.out.println("<" + response.getBody().toString() + ">");
        System.out.println(tester.exists(response, "items"));
        // json node only:
//        System.out.println("->\n" + tester.find(response, "body") + "\n<-");

//        HttpResponse<JsonNode> response1 = testGenerateViaPost("factions", "lawful,good,pick-species,basic");
//        tester.test(response1, 200);

        System.out.println(tester.results());
    }

    private static HttpResponse<String> testEvaluateViaGet(String selector, String tags) {
        RestTool.addTitle("evaluate flow");
        String handle = "user";
        String pass = "user";
        HttpResponse<String> response = RestTool.getStringFromRestServiceBasicAuth("local", "localhost_2112", String.format("/ref/evaluate/%s/%s", selector, tags), handle, pass);
        return response;
    }

    public static void testTags() {
        String select = "race selects from\n" +
                " human=9{pick-species|human}[human,species=human]\n" +
                " elf=4{pick-species|elf}[demihuman,species=elf]\n" +
                " dwarf=5{pick-species|dwarf}[demihuman,species=dwarf]\n" +
                " hobbit=1{pick-species|hobbit}[demihuman,species=hobbit]\n" +
                "\n";
        SelectableItemList selectableItemList = SelectableTokenizer.selectFrom(select.toString());
        System.out.println(selectableItemList.select(new Tags("pick-species")));
    }

    public static HttpResponse<JsonNode> testGenerateViaPost(String template, String tags) throws IOException {
        RestTool.addTitle("basic.catalog Generate workflow");
        String handle = "user";
        String pass = "user";
        Map<String, String> parms = new HashMap<>();
        //parms.put("template", template);

        parms.put("tags", new ArrayList<String>(Arrays.asList(tags.split(","))).toString());
        String json = RestTool.buildJsonBody(parms);
        HttpResponse<JsonNode> response = RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_2112", String.format("/ref/json/%s/1/", template), handle, pass, json);
        return response;
    }

    public static HttpResponse<JsonNode> testGenerate(String template, String tags) {
        RestTool.addTitle("basic.catalog Generate workflow");
        String handle = "user";
        String pass = "user";
        Map<String, String> parms = new HashMap<>();
        parms.put("template", template);
        parms.put("tags", tags);
        String json = RestTool.buildJsonBody(parms);
        HttpResponse<JsonNode> response = RestTool.getJsonFromRestServiceBasicAuth("local", "localhost_2112", String.format("/ref/generate/%s/1/%s", template, tags), handle, pass);
        return response;
    }

    public static HttpResponse<String> testRefTemplateGet(String name) {
        RestTool.addTitle("basic.catalog GET evaluate");
        String handle = "user";
        String pass = "user";
        Map<String, String> parms = new HashMap<>();
        parms.put("body", name);
        String json = RestTool.buildJsonBody(parms); // "/template/{pageName}/{id}"
        return RestTool.getStringFromRestServiceBasicAuth("local", "localhost_2112", String.format("/ref/template/%s/1", name), handle, pass);
    }

    public static HttpResponse<JsonNode> testRefEvaluatePost(String name) throws IOException {
        RestTool.addTitle("basic.catalog POST evaluate");
        String handle = "user";
        String pass = "user";
        Map<String, String> parms = new HashMap<>();
        parms.put("body", name);
        String json = RestTool.buildJsonBody(parms);
        return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_2112", "/ref/evaluate", handle, pass, json);
    }

    public static HttpResponse<String> testRefEvaluateGet(String name) throws IOException {
        RestTool.addTitle("basic.catalog GET evaluate");
        String handle = "user";
        String pass = "user";
        return RestTool.getStringFromRestServiceBasicAuth("local", "localhost_2112", "/ref/evaluate/" + name, handle, pass);
    }

    public static HttpResponse<String> testRefEvaluateTemplatePost(String pageName, String id) {
        RestTool.addTitle("basic.catalog GET evaluate");
        String handle = "user";
        String pass = "user";
        UriComponentsBuilder bldr = UriComponentsBuilder.fromUriString("/ref/template/");
        bldr.pathSegment(pageName, id);
        return RestTool.getStringFromRestServiceBasicAuth("local", "localhost_2112", bldr.toUriString(), handle, pass);
    }

}
