package org.refracktory.refrackt;


import com.knunk.resttest.util.RestTester;
import com.knunk.tools.resttool.RestTool;
import org.springframework.web.util.UriComponentsBuilder;
import unirest.HttpResponse;
import unirest.JsonNode;
import unirest.UnirestException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestBasicFun {

  public static void main(String[] args) throws UnirestException, IOException {
    RestTester tester = new RestTester();

    tester.test(testRefEvaluatePost("color"), 200);
//    tester.testForString(testRefEvaluateGet("color"), 200);
//    tester.testForString(testRefEvaluateTemplatePost("page", "1"), 200);
    System.out.println(tester.results());
  }

  public static HttpResponse<JsonNode> testRefEvaluatePost(String name) throws IOException {
    RestTool.addTitle("basic.catalog POST evaluate");
    String handle = "user";
    String pass = "user";
    Map<String, String> parms = new HashMap<>();
    parms.put("body", name);
    String json = RestTool.buildJsonBody(parms);
    return RestTool.postJsonFromRestServiceBasicAuth("local", "localhost_2112", "/ref/evaluate", handle, pass, json);
  }

  public static HttpResponse<String> testRefEvaluateGet(String name) throws IOException {
    RestTool.addTitle("basic.catalog GET evaluate");
    String handle = "user";
    String pass = "user";
    return RestTool.getStringFromRestServiceBasicAuth("local", "localhost_2112", "/ref/evaluate/" + name, handle, pass);
  }

  public static HttpResponse<String> testRefEvaluateTemplatePost(String pageName, String id) throws IOException {
    RestTool.addTitle("basic.catalog GET evaluate");
    String handle = "user";
    String pass = "user";
    UriComponentsBuilder bldr = UriComponentsBuilder.fromUriString("/ref/template/");
    bldr.pathSegment(pageName, id);
    return RestTool.getStringFromRestServiceBasicAuth("local", "localhost_2112", bldr.toUriString(), handle, pass);
  }

}
