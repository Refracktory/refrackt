package org.refracktory.refrackt;

import com.knunk.resttest.util.RestTester;
import com.knunk.tools.resttool.RestTool;
import unirest.HttpResponse;
import unirest.JsonNode;
import unirest.UnirestException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestShow {

    public static void main(String[] args) throws UnirestException, IOException {
        RestTester tester = new RestTester();

//        tester.testHttpStatus(testRefEvaluateGet("color"), 200);
//        tester.testHttpStatus(testRefEvaluateGet("xyzzy"), 404);
//
//        tester.test(testRefEvaluatePost("texture"), 200);
//        tester.test(testRefEvaluatePost("xyzzy"), 404);
//
//        tester.testHttpStatus(testRefTemplateGet("test"), 200);
//        tester.testHttpStatus(testRefTemplateGet("xyzzy"), 404);
//
//        tester.test(testGenerate("sheet", "lawful,good"), 200);
        tester.test(testShow(), 200);
        System.out.println(tester.results());
    }

    public static HttpResponse<JsonNode> testShow() throws IOException {
        RestTool.addTitle("data show");
        String handle = "user";
        String pass = "user";
        Map<String, String> parms = new HashMap<>();
        String json = RestTool.buildJsonBody(parms);
        HttpResponse<JsonNode> response = RestTool.getHtmlFromRestServiceBasicAuth("local", "localhost_2112", String.format("/ref/show/"), handle, pass);
        return response;
    }

}
