package org.refracktory.refrackt.config;

import com.knunk.util.Console;
import com.knunk.util.Log;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {
    private Log log = Console.console(this);

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //FIXME need some work on CORS here
        log.warn("CORS is being ignored here");
        registry.addMapping("/**");
    }
}