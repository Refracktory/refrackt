package org.refracktory.refrackt.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity(debug = false)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  public void globalUserDetails(AuthenticationManagerBuilder auth, AuthenticationProvider authenticationProvider) throws Exception {
    auth.authenticationProvider(authenticationProvider);
  }

  @Override
  @Order(Ordered.HIGHEST_PRECEDENCE)
  protected void configure(HttpSecurity http) throws Exception {
    logger.info("setting up HTTP security");
    http
      .sessionManagement()
      .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
      .and()
      .csrf().disable()
      .authorizeRequests()
      .antMatchers("/*").permitAll()
      .antMatchers("/ref/**").permitAll()
      .antMatchers("/**").hasAnyAuthority("User", "Admin", "Owner")
//      .antMatchers("/plus/**").authenticated()
//      .antMatchers("/connection/**").authenticated()
//      .antMatchers("/connection/**").hasAnyAuthority("User", "Admin", "Owner")
//      .antMatchers("/admin/**").authenticated()
//      .antMatchers("/admin/**").hasAnyAuthority("Admin", "Owner")
//      .anyRequest().authenticated()
      .and()
      .httpBasic()
      .realmName("CODEX");
  }


  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

}