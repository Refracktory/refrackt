package org.refracktory.refrackt.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
  //private @Autowired MemberRepository memberRepository;
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private PasswordEncoder passwordEncoder;

  // this is the important part:
  @Bean(name = "authenticationProvider")
  public DaoAuthenticationProvider authProvider(@Autowired PasswordEncoder passwordEncoder) {
    final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(getUserDetailsService());
    authProvider.setPasswordEncoder(passwordEncoder);
    return authProvider;
  }

  /** THIS WORKS!! */
  @Bean
  public UserDetailsService getUserDetailsService() {
    UserDetailsService userDetailsService = new UserDetailsService() {
      @Override public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        logger.debug("checking for " + name);
//        UserDetails ud = memberRepository.findMemberPOByHandle(name);
//        if (ud == null) {
//          throw new UsernameNotFoundException("user not found");
//        }
        Map<String, String> userProps = new HashMap<>();
        UserDetails ud = getDefaultUserDetails();
        logger.debug("has authorities: " + ud.getAuthorities());
        return ud;
      }
    };
    return userDetailsService;
  }

  private UserDetails getDefaultUserDetails() {
    return new UserDetails() {
      @Override public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> collection = new ArrayList<>();
        collection.add(new GrantedAuthority() {
          @Override public String getAuthority() {
            return "User";
          }
        });
        return collection;
      }

      @Override public String getPassword() {
        return "user";
      }

      @Override public String getUsername() {
        return "user";
      }

      @Override public boolean isAccountNonExpired() {
        return true;
      }

      @Override public boolean isAccountNonLocked() {
        return true;
      }

      @Override public boolean isCredentialsNonExpired() {
        return true;
      }

      @Override public boolean isEnabled() {
        return true;
      }
    };
  }

  @Bean
  @Profile("insecure")
  public PasswordEncoder getPasswordEncoderInsecure() {
    // FIXME
    logger.warn("using no op password encoder");
    return NoOpPasswordEncoder.getInstance();
    //return new BCryptPasswordEncoder();
  }

  @Bean
  @Profile("default")
  public PasswordEncoder getPasswordEncoderSecure() {
    logger.info("using BCrypt password encoder");
    return new BCryptPasswordEncoder();
  }

  @Override
  public void init(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(getUserDetailsService())
      .passwordEncoder(passwordEncoder);
  }
}