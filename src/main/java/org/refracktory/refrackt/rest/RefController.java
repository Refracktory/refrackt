package org.refracktory.refrackt.rest;

import com.knunk.util.Console;
import com.knunk.util.Log;
import org.json.JSONObject;
import org.refracktory.refrackt.exceptions.GeneratorMissingException;
import org.refracktory.refrackt.exceptions.SelectableMissingException;
import org.refracktory.refrackt.exceptions.TemplateMissingException;
import org.refracktory.refrackt.html.HtmlRenderEngine;
import org.refracktory.refrackt.rest.objects.DataRequest;
import org.refracktory.refrackt.rest.objects.MainRequest;
import org.refracktory.refrackt.rest.objects.MainResponse;
import org.refracktory.refrackt.services.techno.RefracktService;
import org.refracktory.refrackt.services.techno.composer.ComposeService;
import org.refracktory.refrackt.services.techno.composer.Composition;
import org.refracktory.refrackt.services.techno.selectable.SelectableItem;
import org.refracktory.refrackt.services.techno.tags.Tags;
import org.refracktory.refrackt.templates.Template;
import org.refracktory.refrackt.util.Constants;
import org.refracktory.refrackt.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@RestController
@RequestMapping("/ref")
@CrossOrigin(origins = "http://localhost:3000")
public class RefController {

  private Log log = Console.console(this);

  @Autowired
  private RefracktService refracktService;

  @RequestMapping(value = "/evaluate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<MainResponse> evaluate(@RequestBody @Valid MainRequest mainRequest, HttpServletRequest request) {
    log.info("/evaluate[%s]", mainRequest.getBody());
    try {
      String result = refracktService.getTemplate(mainRequest.getBody());
      MainResponse mainResponse = new MainResponse();
      mainResponse.setResult(new String(result.getBytes(), StandardCharsets.UTF_8));
      return ResponseEntity.ok(mainResponse);
    } catch (SelectableMissingException | TemplateMissingException sme) {
      MainResponse mainResponse = new MainResponse();
      mainResponse.setResult(mainRequest.getBody() + " could not be located");
      return new ResponseEntity<MainResponse>(mainResponse, HttpStatus.NOT_FOUND);
    }
  }

  @RequestMapping(value = "/evaluate/{variable}", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<String> evaluate(@PathVariable String variable, HttpServletRequest request) {
    log.info("/evaluate/%s", variable);
    try {
      String result = refracktService.getTemplate(variable);
      return ResponseEntity.ok(new String(result.getBytes(), StandardCharsets.UTF_8));
    } catch (SelectableMissingException | TemplateMissingException sme) {
      return new ResponseEntity<String>(variable + " not found", HttpStatus.NOT_FOUND);
    }
  }

  @RequestMapping(value = "/evaluate/{selector}/{tags}", method = RequestMethod.GET, produces = "application/text;charset=UTF-8") // was plain/text!
  @ResponseBody
  public ResponseEntity<String> evaluateSelectorGet(@PathVariable String selector, @PathVariable String tags, HttpServletRequest request) {
    log.info("/evaluate/%s/%s", selector, tags);
    try {
      String result = refracktService.getASelectorResult(ComposeService.COMPOSITION_DELIMITER + selector + ComposeService.COMPOSITION_DELIMITER, new Tags(tags));
      log.info(result);
      return ResponseEntity.ok(new String(result.getBytes(), StandardCharsets.UTF_8));
    } catch (SelectableMissingException sme) {
      return new ResponseEntity<String>(selector + " not found", HttpStatus.NOT_FOUND);
    }
  }


  @RequestMapping(value = "/evaluate/{selector}/{tags}", method = RequestMethod.POST, produces = "application/text;charset=UTF-8") // was plain/text!
  @ResponseBody
  public ResponseEntity<String> evaluateSelectorPost(@PathVariable String selector, @PathVariable String tags, HttpServletRequest request) {
    log.info("/evaluate/%s/%s", selector, tags);
    try {
      String result = refracktService.getASelectorResult(ComposeService.COMPOSITION_DELIMITER + selector + ComposeService.COMPOSITION_DELIMITER, new Tags(tags));
      log.info(result);
      return ResponseEntity.ok(new String(result.getBytes(), StandardCharsets.UTF_8));
    } catch (SelectableMissingException sme) {
      return new ResponseEntity<String>(selector + " not found", HttpStatus.NOT_FOUND);
    }
  }

  @RequestMapping(value = "/template/{pageName}/{id}", method = RequestMethod.GET, produces = "application/text;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<String> getTemplate(@PathVariable String pageName, @PathVariable String id) {
    log.info("/template/%s/%s", pageName, id);
    try {
      Template result = refracktService.getTemplate(pageName, id);
      return ResponseEntity.ok(result.getText());
    } catch (TemplateMissingException sme) {
      return new ResponseEntity<String>(pageName + "[" + id +"]" + " not found", HttpStatus.NOT_FOUND);
    }
  }

  @RequestMapping(value = "/generate/{pageName}/{id}/{tags}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<String> generatePage(@PathVariable String pageName, @PathVariable String id, @PathVariable String tags) {
    log.info("/template/%s/%s/%s", pageName, id, tags);
    try {
      Tags tags1 = new Tags(tags);
      DataRequest dataRequest = new DataRequest();
      dataRequest.setTags(Arrays.asList(tags1.toList()));
      Composition result = refracktService.generate(pageName, id, dataRequest);
      return ResponseEntity.ok(result.getText());
    } catch (TemplateMissingException | GeneratorMissingException | SelectableMissingException sme) {
      sme.printStackTrace();
      return new ResponseEntity<String>(pageName + "[" + id +"]" + " not found", HttpStatus.NOT_FOUND);
    }
  }

  @RequestMapping(value = "/data/{pageName}/{id}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<DataRequest> generateTable(@PathVariable String pageName, @PathVariable String id,
                                             @RequestBody DataRequest dataRequest, HttpServletRequest request) {
    log.info("/template/%s/%s/%s", pageName, id, dataRequest.getTags(), dataRequest.getProperties());
    try {
      Tags tags = new Tags(dataRequest.getTags());
      Properties properties = new Properties();
      properties.putAll(dataRequest.getProperties());
      Composition result = refracktService.generate(pageName, id, dataRequest);
      DataRequest response = new DataRequest();
      response.setProperties(dataRequest.getProperties());
      response.getProperties().putAll(result.getProperties());
      response.setBody(result.toString());
      return ResponseEntity.ok(response);
    } catch (TemplateMissingException | GeneratorMissingException | SelectableMissingException sme) {
      sme.printStackTrace();
      DataRequest error = new DataRequest();
      error.getProperties().put("errorMessage", sme.getMessage());
      return ResponseEntity.badRequest().body(error);
    }
  }

  @RequestMapping(value = "/json/{pageName}/{id}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
  @ResponseBody
  public ResponseEntity<DataRequest> generateJson(@PathVariable String pageName, @PathVariable String id,
                                                  @RequestBody DataRequest dataRequest, HttpServletRequest request) {
    log.info("/template/%s/%s/%s", pageName, id, dataRequest.getTags(), dataRequest.getProperties());
    try {
      Tags tags = new Tags(dataRequest.getTags());
      Properties properties = new Properties();
      properties.putAll(dataRequest.getProperties());
      Composition result = refracktService.generate(pageName, id, dataRequest);
      DataRequest response = new DataRequest();
      response.setProperties(dataRequest.getProperties());
      response.getProperties().putAll(result.getProperties());
      JSONObject json = TableConverter.toJson(result.getText());
      json.put("table_name", pageName + "_table");
      response.setBody(json.toString());
      return ResponseEntity.ok(response);
    } catch (TemplateMissingException | GeneratorMissingException | SelectableMissingException sme) {
      sme.printStackTrace();
      DataRequest error = new DataRequest();
      error.getProperties().put("errorMessage", sme.getMessage());
      return ResponseEntity.badRequest().body(error);
    }
  }


  @RequestMapping(value = "/show", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
  @ResponseBody
  public ResponseEntity<String> showSelectors(HttpServletRequest request) {
    List<String> list = refracktService.getListOfSelectors();
    StringBuilder sb = new StringBuilder();
    for (String key : list) {
      String link = HtmlRenderEngine.createAnchorWithClass("show/" + key, key, "link");
        sb.append(link + "</BR>\n" + Constants.CR);
    }
    String html = HtmlRenderEngine.getBaseTemplate("show selectors", sb.toString());
    return ResponseEntity.ok(html);
  }

  @GetMapping(
          value = "/show/{selector}",
          consumes = MediaType.ALL_VALUE,
          produces = MediaType.TEXT_HTML_VALUE
  )
  @ResponseBody
  public ResponseEntity<String> showSelector(@PathVariable String selector, HttpServletRequest request) {
    try {
      StringBuilder sb = new StringBuilder();
      sb.append("<h2>" + selector + "</h2>");

      List<SelectableItem> list = refracktService.getDataForSelector(selector);
      Collections.sort(list);

//      sb.append("<PRE>");
      for (SelectableItem item : list) {
        String link = item.toListItem();
        List<String> vars = Utility.parseOutVariables(link);
        for (String var: vars) {
          String attribute;
          if (refracktService.getListOfSelectors().contains(var)) {
            attribute = "link";
          } else {
            attribute = "broken_link";
          }
          link = link.replace(Utility.raise(var), HtmlRenderEngine.createAnchorWithClass("/ref/show/" + var, var, attribute));
        }
        sb.append(link + "</BR>\n" + Constants.CR);
      }

      //      sb.append("</PRE>");
      String html = HtmlRenderEngine.getBaseTemplate("selector " + selector, sb.toString());
      return ResponseEntity.ok(html);
    } catch (SelectableMissingException e) {
      e.printStackTrace();
    }
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, selector + " could not be located");
    }

  @RequestMapping(value = "/show/{selector}/{tags}", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
  @ResponseBody
  public ResponseEntity<String> showSelectorByTags(@PathVariable String selector, @PathVariable String tags, HttpServletRequest request) {
      StringBuilder sb = new StringBuilder();
      List<SelectableItem> list = refracktService.getDataForSelector(selector, new Tags(tags));
//      sb.append("<PRE>");
    Collections.sort(list);
      for(SelectableItem item : list) {
        String link = item.toListItem();
        List<String> vars = Utility.parseOutVariables(link);
        for (String var: vars) {

          String attribute;
          if (refracktService.getListOfSelectors().contains(var)) {
            link = link.replace(Utility.raise(var), HtmlRenderEngine.createAnchorWithClass(var, var, "link"));
          } else {
            link = link.replace(Utility.raise(var), HtmlRenderEngine.createAnchorWithClass(var, var, "broken_link"));
          }
        }
        sb.append(link + "</BR>\n");
      }

      //      sb.append("</PRE>");
      String html = HtmlRenderEngine.getBaseTemplate("selector " + selector, sb.toString());
      return ResponseEntity.ok(html);
    }
  }