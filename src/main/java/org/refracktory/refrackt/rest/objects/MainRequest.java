package org.refracktory.refrackt.rest.objects;

public class MainRequest {
  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  private String body;
}
