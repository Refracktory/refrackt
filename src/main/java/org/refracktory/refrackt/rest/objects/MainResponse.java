package org.refracktory.refrackt.rest.objects;

public class MainResponse {
  private String result;
  private int returnCode;

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public int getReturnCode() {
    return returnCode;
  }

  public void setReturnCode(int returnCode) {
    this.returnCode = returnCode;
  }

}
