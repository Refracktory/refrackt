package org.refracktory.refrackt.rest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

public class TableConverter {
    public static JSONObject toJson(String text) {
        String[] lines = text.split("\n");
        String header = lines[0];
        lines = Arrays.copyOfRange(lines, 1, lines.length);

        JSONObject carrier = new JSONObject();
        String[] names = header.split(" \\| ");
        JSONArray columns = new JSONArray(Arrays.asList(names));
        boolean success = false;
        JSONArray items = new JSONArray();
        for (String line: lines) {
            String[] elements = line.split(" \\| ");
            if (names.length != elements.length) {
                if (success) {
                    break;
                }
                else {
                    throw new IllegalArgumentException(String.format("size of this table element: %s does not match header size: %d", line, names.length));
                }
            }
            JSONArray item = new JSONArray();
            for (int i=0; i < elements.length ; i++) {
                item.put(elements[i]);
            }
            success = true;
            items.put(item);
        }
        carrier.put("columns", columns);
        carrier.put("items", items);
        return carrier;
    }
}
