package org.refracktory.refrackt.html;

import java.util.HashMap;
import java.util.Map;

public class AttributeBuilder {
    private Map<String, String> attributeMap = new HashMap<>();

    public static void main(String[] args) {
        System.out.println(AttributeBuilder.start().attribute("ray", "me").attribute("doe", "you").build());
        System.out.println(AttributeBuilder.start().build());
    }

    private AttributeBuilder() {
        super();
    }

    public static AttributeBuilder start() {
        return new AttributeBuilder();
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        for (String key: attributeMap.keySet()) {
            sb.append(" " + key + "=" + quote(attributeMap.get(key)));
        }
        return sb.toString();
    }

    public AttributeBuilder attribute(String key, String value) {
        attributeMap.put(key, value);
        return this;
    }

    public static String quote(String element) {
        return "\"" + element + "\"";
    }

}
