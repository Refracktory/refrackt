package org.refracktory.refrackt.html;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.annotation.Experimental;

import java.util.Map;

public class HtmlRenderEngine {

    public static void main(String[] args) {
        System.out.println(HtmlRenderEngine.getBaseTemplate("Main Page", "hello, world!"));
    }

    public static String startHtml() {
        return "<html>";
    }

    public static String createStyleSheetLink(String cssFileName) {
        return i(1, "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + cssFileName + "\">");
    }

    public static String createFaviconLink() {
        return i(1, "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico?127\">");
    }

    public static String createHead(String title) {
        return "<head>"
                + i(1, "<title>" + title  + "</title>\n")
                + getInlineCss();
    }

    public static String endHead() {
        return "</head>";
    }
    public static String startBody() {
        return "\n<body>";
    }

    public static String endBody() {
        return "</body>";
    }

    public static String endHtml() {
        return "\n</html>";
    }

    /**
     * This is for rapid prototyping.
     * @return string of the current WIP CSS
     */
    public static String getInlineCss() {
        StringBuilder css = new StringBuilder();
        css.append(i(1,"\n<style>"));
        css.append(i(2,".broken_link {\n") +
                   i(3,"pointer-events.idea: none;\n") +
                   i(3,"cursor: default;\n") +
                   i(3,"color: orange;\n") +
                   i(2,"}\n"));
        css.append(i(1,"</style>"));
        return css.toString();
    }

    public static String getBaseTemplate(String pageTitle, String bodyText) {
        StringBuilder html = new StringBuilder();
        html.append(startHtml());
        html.append(i(1, createHead(pageTitle)));
        html.append(i(1, createStyleSheetLink("/css/application.css")));
        html.append(i(1, createFaviconLink()));
        html.append(i(1, endHead()));
        html.append(i(1, startBody()));
        html.append(i(2, bodyText));
        html.append(i(1, endBody()));
        html.append(i(0, endHtml()));
        return html.toString();
    }

    @Experimental
    public static String i(int count, String text) {
        String[] parts = text.split("\\n");
        StringBuilder sb = new StringBuilder();
        for (String part : parts) {
            String temp = StringUtils.leftPad("", 4*count) + part;
            sb.append("\n" + temp);
        }
        return sb.toString();
    }

    /**
     * messy attribute building
     * @param url
     * @param text
     * @param attributePairs
     * @return
     */
    public static String createAnchor(String url, String text, String...attributePairs) {
        StringBuilder attributes = new StringBuilder();
        if (attributePairs.length > 0) {
            for (String entry : attributePairs) {
               String[] pair = entry.split("=");
               attributes.append(" " + pair[0] + "=" + quote(pair[1]) ) ;
            }
        }
        return "<a" + attributes.toString() + " href=" + quote(url) + " >" + text + "</a>";
    }

    public static String createAnchorWithClass(String url, String text, String className) {
        StringBuilder attributes = new StringBuilder();
        return "<a" + classAttribute(className) + " href=" + quote(url) + " >" + text + "</a>";
    }

    public static String div(String id, String cssClass, String innerText, Map<String, String> extraAttribs) {
        StringBuilder sb = new StringBuilder();

        sb.append(startDiv(id, cssClass, innerText, convertMapToAttributeList(extraAttribs)));
        sb.append(i(1, innerText));
        sb.append("</div");
        return sb.toString();
    }

    /**
     * probably need to create a attribute builder.
     * @param map
     * @return
     */
    public static String convertMapToAttributeList(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        if (!map.isEmpty()) {
            sb.append(" ");
        }
        for (String key: map.keySet()) {
            sb.append(key + "=" + quote(map.get(key)));
        }
        return sb.toString();
    }

    public static String startDiv(String id, String cssClass, String attribs) {
        StringBuilder sb = new StringBuilder();
        sb.append("<div");
        if (id != null) {
            sb.append(" id=" +quote(id));
        }
        if (cssClass != null) {
            sb.append(" class=" + quote(cssClass));
        }
        if (attribs != null) {
            sb.append(" " + attribs);
        }
        sb.append(">\n");
        return sb.toString();
    }

    public static String startDiv(String id, String cssClass, String...extraAttribs) {
        StringBuilder sb = new StringBuilder();
        sb.append("<div");
        if (id != null) {
            sb.append(" id=" +quote(id));
        }
        if (cssClass != null) {
            sb.append(" class=" + quote(cssClass));
        }
        for (String attr: extraAttribs) {
            sb.append(" " + attr);
        }

        sb.append(">\n");
        return sb.toString();
    }

    public static String endDiv() {
        return "</div>";
    }

    public static String classAttribute(String className) {
        return " class=" + quote(className);
    }

    public static String quote(String element) {
        return "\"" + element + "\"";
    }
}
