package org.refracktory.refrackt.repository;

import com.knunk.files.FileFinder;
import com.knunk.util.AbstractPersistentProperties;
import com.knunk.util.Console;
import com.knunk.util.Environment;
import com.knunk.util.Log;
import org.refracktory.refrackt.exceptions.GeneratorAlreadyDefinedException;
import org.refracktory.refrackt.exceptions.GeneratorMissingException;
import org.refracktory.refrackt.exceptions.TemplateMissingException;
import org.refracktory.refrackt.services.techno.files.FileManager;
import org.refracktory.refrackt.services.techno.files.GenerationFile;
import org.refracktory.refrackt.services.techno.generation.Generator;
import org.refracktory.refrackt.services.techno.generation.GeneratorTokenizer;
import org.refracktory.refrackt.templates.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;

@Component
public class GeneratorRepository {
    private Log log = Console.console(this);
    private Map<String, Generator> generators = new HashMap<>();

    @Autowired
    private TemplateRepository templateRepository;

    @Autowired
    private AbstractPersistentProperties applicationProperties;

    @Autowired
    private GeneratorTokenizer generatorTokenizer;

    @PostConstruct
    public void postConstruct() throws GeneratorAlreadyDefinedException {

        String parentDir = Environment.getCurrentDir() + File.separator;
        applicationProperties.setProperty("generators_directory", parentDir); //FIXME

        final String dirName = applicationProperties.get("generators_directory");
        log.info("loading generators from '%s'", dirName);

        File dir = new File(dirName);
        if (dir.exists()) {
            // collect select files.
            List<File> files = FileFinder.findFiles(dir, ".generate");
            log.info("located file generators:");
            for (File f : files) {
                log.info(" %s", f.getAbsolutePath());
            }
            // add to repo.
            for (File f : files) {
                log.info("loading generator from file '%s'", f.getAbsolutePath());
                GenerationFile sf = FileManager.getGenerationFileFrom(parentDir + File.separator + f.getPath());
                String fileContents = sf.getText().replace("\r", "\n");
                Generator generator = generatorTokenizer.generateFrom(f.getName().substring(0, f.getName().indexOf(".")), fileContents);
                add(generator.getName(), generator);

                // testing only:
               //log.info("selected '%s' from %s", selectableItemList.select(new Tags("!bw")), selectableItemList.getName());
            }
            log.info("generators loaded");
        }
    }

    public Generator get(final String name) throws GeneratorMissingException, TemplateMissingException {
        if (generators.containsKey(name)) {
            Generator g = generators.get(name);
            Template template = templateRepository.get(name);
            g.setTemplate(template);
            return g;
        }
        throw new GeneratorMissingException(String.format("Generator '%s' is missing", name));
    }

    public void add(final String name, final Generator generator) throws GeneratorAlreadyDefinedException {
        if (generators.containsKey(name)) {
            throw new GeneratorAlreadyDefinedException(String.format("Generator '%s' is already defined", name));
        }
        generators.put(name, generator);
    }

    public int count() {
        return generators.size();
    }

    public List<String> keySet() {
        List<String> list = new ArrayList<>(generators.keySet());
        Collections.sort(list);
        return list;
    }

}
