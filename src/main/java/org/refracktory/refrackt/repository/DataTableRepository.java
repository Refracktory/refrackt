package org.refracktory.refrackt.repository;

import com.knunk.files.FileFinder;
import com.knunk.util.AbstractPersistentProperties;
import com.knunk.util.Console;
import com.knunk.util.Environment;
import com.knunk.util.Log;
import org.refracktory.refrackt.exceptions.DataTableAlreadyDefinedException;
import org.refracktory.refrackt.exceptions.DataTableMissingException;
import org.refracktory.refrackt.services.techno.datatable.DataTable;
import org.refracktory.refrackt.services.techno.datatable.DataTableTokenizer;
import org.refracktory.refrackt.services.techno.files.FileManager;
import org.refracktory.refrackt.services.techno.files.GenerationFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;

@Component
public class DataTableRepository {

    private Log log = Console.console(this);
    private Map<String, DataTable> datatables = new HashMap<>();

    @Autowired
    private AbstractPersistentProperties applicationProperties;

    @PostConstruct
    public void postConstruct() throws DataTableAlreadyDefinedException {

        String parentDir = Environment.getCurrentDir() + File.separator;
        applicationProperties.setProperty("datatable_directory", parentDir); //FIXME

        final String dirName = applicationProperties.get("datatable_directory");
        log.info("loading datatables from '%s'", dirName);

        File dir = new File(dirName);
        if (dir.exists()) {
            // collect select files.
            List<File> files = FileFinder.findFiles(dir, ".data");
            log.info("located %d file datatables:", files.size());
            for (File f : files) {
                log.info(" %s", f.getAbsolutePath());
            }
            // add to repo.
            for (File f : files) {
                log.info("loading datatable from file '%s'", f.getAbsolutePath());
                String dataName = f.getName().substring(0, f.getName().indexOf(".data"));
                GenerationFile sf = FileManager.getGenerationFileFrom(parentDir + File.separator + f.getPath());
                String fileContents = sf.getText().replace("\r", "\n");
                if (fileContents.trim().length() > 0) {
                    DataTable dataTable = DataTableTokenizer.dataizeFrom(fileContents);
                    if (dataName != null) {
                        if (dataTable.hasReferencedValues()) {
                        }
                        dataTable.setName(dataName);
                    }
                    if (dataTable.isCustom()) {
                        log.info("adding custom version");
                    }
                    add(dataTable.getName(), dataTable);
                } else {
                    log.warn("'%s' was empty - no data loaded", f.getName());
                }
            }
            log.info("%d datatables loaded", datatables.size());
        }
    }

    public DataTable get(final String name) throws DataTableMissingException {
        if (datatables.containsKey(name)) {
            return datatables.get(name);
        }
        throw new DataTableMissingException(String.format("DataTable '%s' is missing", name));
    }

    public void add(final String name, final DataTable dataTable) throws DataTableAlreadyDefinedException {
        if (datatables.containsKey(name)) {
            throw new DataTableAlreadyDefinedException(String.format("DataTable '%s' is already defined"));
        }
        datatables.put(name, dataTable);
    }

    public int count() {
        return datatables.size();
    }

    public List<String> keySet() {
        List<String> list = new ArrayList<>(datatables.keySet());
        Collections.sort(list);
        return list;
    }

    public boolean contains(String dataName) {
        return datatables.containsKey(dataName);
    }
}
