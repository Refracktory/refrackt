package org.refracktory.refrackt.repository;

import com.knunk.files.FileFinder;
import com.knunk.util.*;
import org.refracktory.refrackt.Constants;
import org.refracktory.refrackt.exceptions.SelectableAlreadyDefinedException;
import org.refracktory.refrackt.exceptions.SelectableMissingException;
import org.refracktory.refrackt.services.techno.files.FileManager;
import org.refracktory.refrackt.services.techno.files.GenerationFile;
import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
import org.refracktory.refrackt.services.techno.selectable.SelectableTokenizer;
import org.refracktory.refrackt.services.techno.tags.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;

@Component
public class SelectablesRepository {

  private Log log = Console.console(this);
  private Map<String, SelectableItemList> selectables = new HashMap<>();

  @Autowired
  private AbstractPersistentProperties applicationProperties;

  @PostConstruct
  public void postConstruct() throws SelectableAlreadyDefinedException {

    String parentDir = Environment.getCurrentDir() + File.separator + "files" + File.separator;
    applicationProperties.setProperty("selectables_directory", parentDir); //FIXME

    final String dirName = applicationProperties.get("selectables_directory");
    log.info("loading selectables from '%s'", dirName);

    File dir = new File(dirName);
    if (dir.exists()) {
      // collect select files.
      List<File> files = FileFinder.findFiles(dir, ".sel");
      log.info("located file selectors:");
      for (File f : files) {
        log.info(" %s", f.getAbsolutePath());
      }
      // add to repo.
      for (File f : files) {
        log.info("loading selector from file '%s'", f.getAbsolutePath());
        GenerationFile sf = FileManager.getSelectorFromFile(parentDir + File.separator + f.getPath());
        String fileContents = sf.getText().replace("\r", "\n");
        if (fileContents.trim().startsWith("@skip")) {
          log.info("skipping this selector as it is marked @skip");
        } else if (Parser.matchesWithRegex(fileContents, Constants.SELECT_1)) {
          log.info("style 1 selector");
        } else if (Parser.matchesWithRegex(fileContents, Constants.SELECT_2)) {
          log.info("style 2 selector");
        } else if (Parser.matchesWithRegex(fileContents, Constants.SUBSELECT)) {
          log.info("subselector");
        } else {
          String fullName = f.getName();
          fullName = fullName.substring(0, fullName.indexOf(".sel"));
          fileContents =  fullName + " selects from\n" + fileContents;
        }

        //SelectableItemList selectableItemList = SelectParser.parse(f.getName(), fileContents);
        SelectableItemList selectableItemList = SelectableTokenizer.selectFrom(fileContents);
        if (add(selectableItemList.getName(), selectableItemList)) {
          log.info("added %s", selectableItemList.getName());
        } else {
          String namespace;
          if (f.getPath().contains(File.separator)) {
            namespace = f.getPath();
            namespace = namespace.replace(File.separator, ".");
            namespace = namespace.substring(0 , namespace.indexOf(".sel"));
            //TODO use smallest unique namespace possible
            log.info("adding namespace to %s", selectableItemList.getName());
            add(namespace, selectableItemList);
          } else {
            log.warn("duped selector '%s' without unique namespace", f.getName());
          }
        }

        // testing only:
        log.info("selected '%s' from %s", selectableItemList.select(new Tags()), selectableItemList.getName());
      }
      log.info("selectors loaded");
    }

    // catalogs of selectors:
    applicationProperties.setProperty("catalog_directory", parentDir); //FIXME
    final String catalogDirName = applicationProperties.get("catalog_directory");
    log.info("loading selector catalog from '%s'", catalogDirName);
    dir = new File(catalogDirName);
    if (dir.exists()) {
      // collect select files.
      List<File> files = FileFinder.findFiles(dir, ".catalog");
      log.info("located selector catalog:");
      for (File f : files) {
        log.info(" %s", f.getAbsolutePath());
      }
      // add to repo.
      for (File f : files) {
        log.info("loading selectors from catalog '%s'", f.getAbsolutePath());
        String fileContents = FileUtil.readFileWithCR(parentDir + File.separator + f.getPath());
        fileContents = fileContents.replace("\r", "\n");
        String[] lines = fileContents.split("\n");

        boolean insideElement = false;
        StringBuilder elementText = new StringBuilder();
        for (String line : lines) {
          if (Parser.matchesWithRegex(line, Constants.SELECT_1)) {
            log.info("style 1 selector");
            if (insideElement) {
              createSelector(elementText);
//              log.info("selected '%s' from %s", selectableItemList.select(new Tags("!bw")), selectableItemList.getName());
              elementText = new StringBuilder();
            }
            elementText.append(line + "\n");
            insideElement = true;
          } else if (Parser.matchesWithRegex(line, Constants.SELECT_2)) {
            log.info("style 2 selector");
            if (insideElement) {
              SelectableItemList selectableItemList = SelectableTokenizer.selectFrom(elementText.toString());
              add(selectableItemList.getName(), selectableItemList);
//              log.info("selected '%s' from %s", selectableItemList.select(new Tags()), selectableItemList.getName());
              elementText = new StringBuilder();
            }
            elementText.append(line + "\n");
            insideElement = true;
          } else if (Parser.matchesWithRegex(line, Constants.SUBSELECT)) {
            log.info("subselector: %s", line);
            // this currently uses single line format
            String parentSelectable = Parser.afterParse(line, ") from ");
            SelectableItemList full = selectables.get(parentSelectable);
            if (full != null) {
              String regex = Parser.tweenParse(line, "subselects(", ") from");
              String name = line.substring(0, line.indexOf(" subselects"));
              SelectableItemList result = full.subselect(name, regex);
              add(result.getName(), result);
            } else {
              log.warn("could not find parent selector '%s'", parentSelectable );
            }
          } else if (Parser.matchesWithRegex(line, Constants.LONGSELECT)) {
            SelectableItemList selectableItemList = SelectableTokenizer.selectFrom(line);
            add(selectableItemList.getName(), selectableItemList);
          } else if (Parser.matchesWithRegex(line, "^#.*")) {
            // comment
            log.info("%s", line);
          } else if (line.trim().length() == 0) {
            //log.info("skip blank line");
          } else if (insideElement) {
            log.info(":%s", line);
            elementText.append(line + "\n");
          }
        }
        if (elementText.length() > 0) {
          createSelector(elementText);
        }
      }
    }
  }

  public void createSelector(StringBuilder elementText) throws SelectableAlreadyDefinedException {
    SelectableItemList selectableItemList = SelectableTokenizer.selectFrom(elementText.toString());
    if (add(selectableItemList.getName(), selectableItemList)) {
      log.info("added %s", selectableItemList.getName());
    } else {
      log.warn("did not add dupe - you need to use namespaces...");
    }
  }

  public SelectableItemList get(final String name) throws SelectableMissingException {
    if (selectables.containsKey(name)) {
      SelectableItemList list = selectables.get(name);
      SelectableItemList returnable = list.makeCopy();
      return returnable;
    }
    //return null;
    throw new SelectableMissingException(String.format("Selectable '%s' is missing", name));
  }

  public boolean add(final String name, final SelectableItemList selectableItemList) throws SelectableAlreadyDefinedException {
    if (selectables.containsKey(name)) {
      //throw new SelectableAlreadyDefinedException(String.format("Selectable '%s' is already defined", name));
      log.error("Selectable '%s' is already defined", name);
    } else {
      selectables.put(name, selectableItemList);
      return true;
    }
    return false;
  }

  public int count() {
    return selectables.size();
  }

  public List<String> keySet() {
    List<String> list = new ArrayList<>(selectables.keySet());
    Collections.sort(list);
    return list;
  }
}
