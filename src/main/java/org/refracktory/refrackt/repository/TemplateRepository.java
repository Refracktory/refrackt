package org.refracktory.refrackt.repository;

import com.knunk.files.FileFinder;
import com.knunk.util.*;
import org.refracktory.refrackt.exceptions.TemplateAlreadyDefinedException;
import org.refracktory.refrackt.exceptions.TemplateMissingException;
import org.refracktory.refrackt.templates.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@Component
public class TemplateRepository {

  private Log log = Console.console(this);
  private Map<String, Template> templates = new HashMap<>();

  @Autowired
  private AbstractPersistentProperties applicationProperties;

  @PostConstruct
  public void postConstruct() throws TemplateAlreadyDefinedException, IOException {

    String parentDir = Environment.getCurrentDir() + File.separator;
    applicationProperties.setProperty("templates_directory", parentDir); //FIXME
    final String dirName = applicationProperties.get("templates_directory");
    log.info("loading templates from '%s'", dirName);

    File dir = new File(dirName);
    if (dir.exists()) {
      // collect select files.
      List<File> files = FileFinder.findFiles(dir, ".template");
      log.info("located file templates:");
      for (File f : files) {
        log.info(" %s", f.getAbsolutePath());
      }
      // add to repo.
      for (File f : files) {
        log.info("loading templates from file '%s'", f.getAbsolutePath());
          String fullName = f.getName();
          fullName = fullName.substring(0, fullName.indexOf(".template"));
          String templateText = FileUtil.readFileWithCR(f.getAbsolutePath());

          String formatting = f.getParent() + File.separator + fullName + ".formatting";
          File formatFile = new File(dir, formatting);
          Template template = new Template(fullName, templateText);
          if (formatFile.exists()) {
            Properties formatProperties = new Properties();
            formatProperties.load(new FileReader(formatFile));
            template.addFormatting(formatProperties);
            log.info("adding formatting");
          }
          add(fullName, template);
      }
      log.info("templates loaded");
    }
  }

  public Template get(final String name) throws TemplateMissingException {
    if (templates.containsKey(name)) {
      return templates.get(name);
    }
    throw new TemplateMissingException(String.format("Template '%s' is missing", name));
  }

  public void add(final String name, final Template template) throws TemplateAlreadyDefinedException {
    if (templates.containsKey(name)) {
      throw new TemplateAlreadyDefinedException(String.format("Template '%s' is already defined", name));
    }
    templates.put(name, template);
  }

  public int count() {
    return templates.size();
  }

  public List<String> keySet() {
    List<String> list = new ArrayList<>(templates.keySet());
    Collections.sort(list);
    return list;
  }

}
