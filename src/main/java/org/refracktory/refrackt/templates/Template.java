package org.refracktory.refrackt.templates;

import java.util.Properties;

public class Template {
    private String name;
    private String text;
    private Properties formatting = new Properties();

    public Template(String name, String text) {
        this.name = name;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public void addFormatting(Properties formatProperties) {
        this.formatting.putAll(formatProperties);
    }

    public Properties getFormatting() {
        return formatting;
    }
}
