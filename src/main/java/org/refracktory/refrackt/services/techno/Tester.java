package org.refracktory.refrackt.services.techno;

import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
import org.refracktory.refrackt.services.techno.selectable.SelectableTokenizer;
import org.refracktory.refrackt.services.techno.selectable.Selected;
import org.refracktory.refrackt.services.techno.tags.Tags;

public class Tester {
    public static void main(String[] args) {
        Tester.testMore();
//        Tester.testBasic();
    }

    private static void testMore() {
        String text = "mystical-adjective selects from\n" +
                " glowing=1\n" +
                " resonating=1\n" +
                " humming=1\n" +
                " luminous=1{divine,aging=+1}\n" +
                " radiant=1{divine}[spiritual,aging=+2]\n" +
                " pulsing=1\n" +
                " chiming=1{sad&divine}\n" +
                " whining=1\n" +
                " wailing=1{sad|divine}[supernatural,aging=+3]\n" +
                " keening=1{sad&!divine}[supernatural]\n" +
                " moaning=1";
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        Selected selected = list.select(new Tags("divine"));
        System.out.println(selected);
        selected.getGeneratedTags().getAssignments();

    }

    private static void testBasic() {
        String text = "mystical-adjective selects from\n" +
                " glowing=1\n" +
                " resonating=1\n" +
                " humming=1\n" +
                " luminous=1{divine}\n" +
                " radiant=100{divine}\n" +
                " pulsing=1\n" +
                " chiming=1\n" +
                " whining=1\n" +
                " wailing=1{sad}\n" +
                " keening=1{sad}\n" +
                " moaning=1";
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        System.out.println(list.select(new Tags("sad")));
    }
}
