package org.refracktory.refrackt.services.techno.files;

import com.knunk.util.Console;
import com.knunk.util.FileUtil;
import com.knunk.util.Log;
import com.knunk.util.Parser;

import java.io.File;

public class FileManager {
    private static Log log = Console.console(FileManager.class);
    public static String SELECT_1 = "^select ([a-zA-Z0-9_\\-]+) from";
    public static String SELECT_2 = "^([a-zA-Z0-9_\\-]+) selects from"; // support alternate grammar

    public static GenerationFile getSelectorFromFile(String fullFileName) {
        try {

            String selectorName = fullFileName;
            selectorName = selectorName.substring(selectorName.lastIndexOf(File.separator)+1);
            selectorName = selectorName.substring(0, selectorName.indexOf("."));

            String text = FileUtil.readFileIntoStringRemovingPropertyLikeStuff(fullFileName);
            // windows processing
            text = text.replace("\r", "\n");
            if (Parser.matchesWithRegex(text, SELECT_1)) {
                log.info("style 1 selector");
            } else if (Parser.matchesWithRegex(text, SELECT_2)) {
                log.info("style 2 selector");
            } else {
                text = selectorName + " selects from\n" + text;
            }

            log.info("loading selector '%s'", selectorName);
            return new GenerationFile(selectorName, text);
        } catch (Exception e) {
            throw new RuntimeException(String.format("getting file based selector '%s' failed", fullFileName), e);

        }
    }

    public static GenerationFile getGenerationFileFrom(String fullFileName) {
        String selectorName = fullFileName;
        selectorName = selectorName.substring(selectorName.lastIndexOf(File.separator)+1);
        selectorName = selectorName.substring(0, selectorName.indexOf("."));

        String text = FileUtil.readFileWithCR(fullFileName);
        // windows processing
        log.debug("loaded file '%s'", selectorName);
        return new GenerationFile(selectorName, text);
    }
}
