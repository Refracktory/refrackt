package org.refracktory.refrackt.services.techno.parser.tokenizer;

import com.knunk.util.Console;
import com.knunk.util.Log;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tokenizer
{
  private Log log = Console.console(this);
  private LinkedList<TokenInfo> tokenInfos = new LinkedList<TokenInfo>();
  private LinkedList<Token> tokens = new LinkedList<Token>();

  public LinkedList<Token> getTokens() {
    return tokens;
  }

  public void add(String regex, int token) {
    tokenInfos.add(new TokenInfo(
        Pattern.compile("^("+regex+")"), token));
  }
  public void tokenize(String text) {
    String s = new String(text);
    tokens.clear();
    while (!s.equals("")) {
      boolean match = false;

      for (TokenInfo info : tokenInfos) {
        Matcher m = info.regex.matcher(s);
        if (m.find()) {
          match = true;
          String tok = m.group();
          tokens.add(new Token(info.token, tok));
          log.info(String.format("%02d |%s|",info.token, tok.replace("\n", "•")));
          s = m.replaceFirst("");
          break;
        }
      }
      if (!match) {
        throw new TokenizerException("\nUnexpected character in input: >" + s + "<");
      }
    }
  }

  public String detokenize() {
    StringBuilder sb = new StringBuilder();
    for (Token token: tokens) {
      sb.append(token.sequence);
    }
    return sb.toString();
  }
  private class TokenInfo {
    public final Pattern regex;
    public final int token;

    public TokenInfo(Pattern regex, int token) {
      super();
      this.regex = regex;
      this.token = token;
    }
  }

}