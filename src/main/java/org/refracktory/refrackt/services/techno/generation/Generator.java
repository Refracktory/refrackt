package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.Console;
import com.knunk.util.Log;
import org.refracktory.refrackt.services.techno.composer.Composition;
import org.refracktory.refrackt.templates.Template;

import java.util.ArrayList;
import java.util.List;

public class Generator {

    private Log log = Console.console(this);
    private String name;
    private List<Command> commands = new ArrayList<>();
    private Template template;

    public Generator(String name, String body) {
        this.name = name;
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    private String body;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GenerationContext compile(CommandExecutor commandExecutor, GenerationContext gc) {
        log.info("compiling %s", name);

        Generation g = new Generation(template);
        GenerationSet gs = g.startGeneration(gc.getTags());

        for (Command command : commands) {
            log.info("---> executing: %s with %s", command.getActualText(), gc.getTags().toMiniString());
            CommandEvaluation ce = commandExecutor.evaluate(command, gc);
            gc.addProperties(ce.getProperties());

            if (GenerationCommandEnum.CLEAR_TAGS == ce.getStatus()) {
                gc.resetTagsTo(g.getStartingTags());
            } else if (GenerationCommandEnum.ADD_TAGS == ce.getStatus()) {
                gc.addTags(ce.getTags());
            }
            // gs = gs.next(); // not being used...
        }

        //FIXME return compilation unit OR some gen set.
        log.info(gc.getProperties().toString());
        return gc.clone();
    }

    private void log(Command command) {
        log.info(command.getCompiledText());
    }

    public void add(Command command) {
        commands.add(command);
    }

    public void displayLog() {
//        log.info("--------------");
//        log.info("generation log:");
//        for (Command command : commands) {
//            log.info("compiled '%s'", command.getCompiledText());
//        }
//        log.info("--------------");
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public Composition compose(GenerationContext gc) {
        Composition composition = new Composition();
        composition.setTemplate(template);
        composition.setGenerationContext(gc);
        composition.compose();
        return composition;
    }
}
