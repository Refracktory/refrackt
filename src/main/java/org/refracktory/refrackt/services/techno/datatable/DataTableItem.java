package org.refracktory.refrackt.services.techno.datatable;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Token;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Tokenizer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class DataTableItem {
    private static Log log = Console.console(DataTableItem.class);

    private String name;
    private String dataType;
    private String description;
    private Map<String, String> map = new HashMap<>();
    private String referencedBy;

    public static void main(String[] args) {
        DataTableItem.convert("plate is armor with AC=9|absorb=5|deflect=5|deslash=5|weight=45|cost=1000|class=heavy");
        DataTableItem.convert("leather is light and flexible armor with AC=3|absorb=1|deflect=0|deslash=1|weight=10|cost=10|class=light");
        DataTableItem.convert("mail is medium armor with AC=5|absorb=1|deflect=1|deslash=5|weight=13|cost=100|class=medium");
    }

    public static DataTableItem convert(String sequence) {
        Tokenizer tokenizer = getDataTableItemTokenizer();
        tokenizer.tokenize(sequence);
        LinkedList<Token> list =  tokenizer.getTokens();
        String name = null;

        DataTableItem dataTableItem = new DataTableItem();
        String dataType = null;
        String referencedBy = null;
        for (Token token: list) {
            if (token.token == 0 && name == null) {
                log.info("0");
                name = token.sequence.substring(0, token.sequence.indexOf(" is"));
                if (name.contains("[") && referencedBy == null) {
                    referencedBy = Parser.tweenParse(name, "[", "]");
                    dataTableItem.setReferencedBy(referencedBy);
                    log.info("set referenced by to '%s'", referencedBy);
                    name = name.substring(0, name.indexOf("["));
                }
                dataTableItem.setName(name);
            } else if (token.token == 1 && dataType == null) {
                 log.info("1");
                dataType = Parser.getWordBefore(token.sequence, " with ");
                dataTableItem.setType(dataType);
                int end = Math.max(0, token.sequence.indexOf(" " + dataType + " with "));
                String dataDesc = token.sequence.substring(0, end);
                dataTableItem.setDescription(dataDesc);
            } else if (token.token == 2) {
                String[] entries = token.sequence.split("\\|");
                for (String entry : entries) {
                    String[] keyValue = entry.split("=");
                    if (keyValue.length == 2) {
                        dataTableItem.put(keyValue[0], keyValue[1]);
                    } else if (keyValue.length == 1) {
                        dataTableItem.put(keyValue[0], "");
                    }
                }
            } else {
                log.info("%s", token.token);
            }
        }
        if (name == null) {
            throw new IllegalArgumentException("could not determine name of data item from:" + sequence);
        }
        log.info(dataTableItem.toString());
        return dataTableItem;
    }

    private void setReferencedBy(String referencedBy) {
        this.referencedBy = referencedBy;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setType(String dataType) {
        this.dataType = dataType;
    }

    private void put(String key, String value) {
        map.put(key, value);
    }

    private void setName(String name) {
        this.name = name;
    }

    public static Tokenizer getDataTableItemTokenizer() {
        Tokenizer tokenizer = new Tokenizer();

        tokenizer.add("([a-zA-Z0-9_\\-\u0020\\[\\]]+) is ", 0);
        tokenizer.add("([a-zA-Z0-9_\\-, ]+) with ", 1);
        tokenizer.add("([a-zA-Z0-9_\\-|=~\\*\\.]+)", 2);
        tokenizer.add("^[\n][\\u0020]", 10);
        tokenizer.add("[\n|\r]", 13); // whitespace and end of lines.
        return tokenizer;
    }

    public String getName() {
        return name;
    }

    public String getDataType() {
        return dataType;
    }

    public String getDescription() {
        return description;
    }

    public String getReferencedBy() {
        return referencedBy;
    }

    public String find(String attributeName) {
        if (map.containsKey(attributeName)) {
            return map.get(attributeName);
        }
        //FIXME NO IDEA IF THIS IS BEST:
        return "";
    }

    @Override
    public String toString() {
        return String.format("%s %s, %s : %s", name, dataType, description, map.entrySet());
    }
}
