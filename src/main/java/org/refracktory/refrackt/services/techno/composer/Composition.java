package org.refracktory.refrackt.services.techno.composer;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import com.knunk.util.TextUtility;
import org.apache.commons.lang3.StringUtils;
import org.refracktory.refrackt.services.techno.generation.GenerationContext;
import org.refracktory.refrackt.services.techno.render.Render;
import org.refracktory.refrackt.templates.Template;
import org.refracktory.refrackt.util.Utility;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public class Composition {

  private Log log = Console.console(this);
  private String composition = "";
  private Template template;
  private GenerationContext generationContext;

  public String getComposition() {
    return composition;
  }

  public Composition setComposition(String composition) {
    this.composition = composition;
    return this;
  }

  public Composition() {
    super();
  }

  public String getText() {
    return composition;
  }

  public void setTemplate(Template template) {
    this.template = template;
  }

  public void setGenerationContext(GenerationContext gc) {
    this.generationContext = gc;
  }

  public String compose() {
    // FIXME add all the TextBasedRender stuff here.
    String raw = template.getText();
    Properties format = template.getFormatting();
    raw = handleVariables(raw, format);
    raw = raw.replace("\r", "\n");
    raw = handleTextBased(raw);
    log.info("created:\n%s", toString());
    log.info("current tags:%s", generationContext.getTags().asList());
    this.composition = raw;
    return composition;
  }

  private String handleTextBased(String raw) {
    String result = TextUtility.replaceEnglishStuff(raw);
    return result;
    // Breaks stuff: return new MathsTokenizer().tokenize(result);
  }

  /**
    handle substitution including formatted.
   */
  private String handleVariables(String raw, Properties format) {
    List<String> vars = Utility.parseOutVariables(raw);
    for (String var: vars) {
      String value = null;
      if (generationContext.containsKey(var)) {
        value = generationContext.get(var);
      }
      if (value == null) {
        log.warn("var '%s' could not be resolved within '%s' and probably needs added to '.generate' - setting to '•'", var, raw);
        value = "•";
      }
      if (format.containsKey(var)) {
        raw = raw.replace(Utility.raise(var), autoFormat(format.get(var).toString(), value));
      } else {
        raw = raw.replace(Utility.raise(var), value);
      }
    }
    return raw;
  }

  /**
   * Method of using simple java formatters to convert string values into a better 'typed' format
   * this handles some data noise.
   *
   * it would be nice if there was a human format like 0000.00 instead of %6.2f
   *
   * @param format
   * @param text
   * @return
   */
  private String autoFormat(String format, String text) {
    if (text == null) {
      Integer size = Parser.findFirstNumber(format);
      if (size != null) {
        return StringUtils.leftPad(" ", size);
      }
      return "";
    } else if (format.contains("d")) {
      return String.format(format, Long.valueOf(text));
    } else if (format.contains("f")) {
      return String.format(format, Double.valueOf(text));
    } else if (format.contains("s")) {
      return String.format(format, text);
    }
    // this is probably bad
    return String.format(format, text);
  }

  public Map<String, String> getProperties() {
    return generationContext.getProperties().toMap();
  }

  @Override
  public String toString() {
    return new Render().rend(composition);
  }
}
