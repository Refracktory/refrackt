package org.refracktory.refrackt.services.techno.tags;

import com.knunk.util.Console;
import com.knunk.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Assignments {
    transient private Log log = Console.console(this);
    private List<String> assignments = new ArrayList<>();
    public void put(String assignment) {
        log.info("placing %s", assignment);
        assignments.add(assignment);
    }

    @Override
    public String toString() {
        return "Assignments{" + assignments + '}';
    }

    public Properties getAssignmentProperties() {
        Properties p = new Properties();
        for (String assignment: assignments) {
            String key = assignment.substring(0, assignment.indexOf("="));
            String value = assignment.substring(assignment.indexOf("=") + 1);
            p.put(key, value);
        }
        return p;
    }

    public String toMiniString() {
        return assignments != null && assignments.size() > 0 ? assignments.toString() : "";
    }

    public boolean exist() {
        return assignments.size() > 0;
    }
}
