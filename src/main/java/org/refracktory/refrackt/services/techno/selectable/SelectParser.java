package org.refracktory.refrackt.services.techno.selectable;

import com.knunk.util.Console;
import com.knunk.util.CountingMap;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import org.apache.commons.lang3.StringUtils;
import org.refracktory.refrackt.services.techno.tags.Tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectParser {

    private static Log log = Console.console(SelectParser.class);

    public static SelectableItemList parse(final String defaultName, final String text) {

        List<String> lines = getLines(text);
        String selectVarName = getName(defaultName, lines);
        List<String> data = getDataOnly(lines);

        log.info("Selectable: %s", selectVarName);
        List<SelectableItem> list = new ArrayList<>();
        for (String line : data) {
            if (line.trim().length() > 0) {
                SelectableItem item = SelectableItem.convert(line);
                if (item != null) {
                    log.info(" %s", item);
                    list.add(item);
                }
            }
        }
        SelectableItemList itemList = new SelectableItemList(selectVarName, list);
        return itemList;
    }

    private static List<String> getLines(String text) {
        return new ArrayList(Arrays.asList(text.split("\r")));
    }

    private static List<String> getDataOnly(List<String> lines) {
        if (lines.get(0).contains("select ") && lines.get(0).contains(" from")) {
            lines.remove(0);
        }
        return lines;
    }

    private static String getName(String defaultName, List<String> lines) {
        String firstLine = lines.get(0);

        if (firstLine.contains("select ") && firstLine.contains(" from")) {
            return Parser.tweenParse(firstLine, "select ", " from");
        } else if (StringUtils.isNotEmpty(defaultName)) {
            return defaultName;
        }
        return "default";
    }

    public static void main(String...args) {
        //final String complex = "select color from \n red=1\n blue=2\n green=5{old}\n yellow=6{new}\n purple=3{new|old}[royal]\n pink=3{!new&!old}[princess]\n black=1[sith]";
        //String complex = "select color from white=1\n green=5{old}\n yellow=6{new}\n purple=3{new|old}[royal]\n pink=3{!new&!old}[princess]\n black=1{dark}[sith]";
        String complex = "select color from white=1^green=5{old}^yellow=6{new}^purple=3{new|old}[royal]^pink=3{!new&!old}[princess]^black=1{dark}[sith]";
        SelectableItemList selectableItemList = SelectParser.parse("default", complex);
        CountingMap cm = new CountingMap();
        for (int i=0; i < 20; i++) {
            Selected res = selectableItemList.select(new Tags("new,dark"));
            cm.add(res.getText());
            System.out.println(res);
        }
        System.out.println(cm.entrySet());
    }
}
