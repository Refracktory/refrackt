package org.refracktory.refrackt.services.techno.todo;

public class CollectsFrom {
    // names collects 10 from culture-names/whole-name
    // this creates a list or array.
    // useful for generating things like equipment in a backpack.
    // needs two versions.
    // one adds items into the 'same' item in the list
    // one only selects unique items not yet in the list.
}
