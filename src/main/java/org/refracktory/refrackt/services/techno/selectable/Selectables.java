//package org.refracktory.refrackt.services.techno.selectable;
//
//import com.knunk.util.Console;
//import com.knunk.util.Log;
//
//import org.refracktory.refrackt.services.techno.selectable.SelectParser;
//import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
//import org.refracktory.refrackt.services.techno.selectable.Selected;
//import org.refracktory.refrackt.services.techno.tags.Tags;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Hold all Selectables that have been loaded.
// */
//public class Selectables {
//
//    private Log console = Console.console(this.getClass());
//    private Map<String, SelectableItemList> map = new HashMap<>();
//
////    public void add(final String text) {
////        SelectableItemList list = SelectParser.parse(text);
////        add(list);
////    }
//
//    public void add(final SelectableItemList selectableItemList) {
//        map.put(selectableItemList.getName(), selectableItemList);
//    }
//
//    public Selected select(final String name, final Tags tags) {
//        if (map.containsKey(name)) {
//            SelectableItemList selectable = map.get(name);
//            return selectable.select(tags);
//        } else {
//            String res = String.format("could not locate selectable : %s", name);
//            console.debug(res);
//            return new Selected(name, "*", new Tags("error_condition:" + res));
//        }
//    }
//}
