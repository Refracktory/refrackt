package org.refracktory.refrackt.services.techno.generation;

public class Commented extends Command {

    public Commented(String text) {
        super();
        setActualText(text);
    }

    @Override
    public void execute(GenerationContext gc) {
        log.info("#%s", getActualText());
    }
}
