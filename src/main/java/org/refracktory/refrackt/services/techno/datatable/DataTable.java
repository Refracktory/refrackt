package org.refracktory.refrackt.services.techno.datatable;

import org.refracktory.refrackt.services.techno.generation.Command;
import org.refracktory.refrackt.services.techno.generation.GenerationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataTable extends Command {
    private String name;
    private Map<String, DataTableItem> itemMap = new HashMap<>();
    private String dataType;

    private String referencedBy;

    public DataTable(String name, List<DataTableItem> elements) {
        this.name = name;
        for (DataTableItem dataTableItem : elements) {
            itemMap.put(dataTableItem.getName(), dataTableItem);
        }
    }

    public String getDataType() {
        return dataType;
    }

    @Override
    public String toString() {
        return String.format("%s", itemMap.entrySet());
    }

    public String getName() {
        return name;
    }

    @Override
    public void execute(GenerationContext gc) {
        log.warn("not implemented");
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public boolean isCustom() {
        return "custom".equals(dataType);
    }

    public void setReferencedBy(String referencedBy) {
        this.referencedBy = referencedBy;
    }

    public String getReferencedBy() {
        return referencedBy;
    }

    public boolean hasReferencedValues() {
        return referencedBy != null;
    }

    public String find(String name, String attribute) {
        if (itemMap.containsKey(name)) {
            DataTableItem lookup = itemMap.get(name);
            return lookup.find(attribute);
        }
        //FIXME: HANDLE THIS:
        return "-f";
    }

    public void setName(String name) {
        this.name = name;
    }
}
