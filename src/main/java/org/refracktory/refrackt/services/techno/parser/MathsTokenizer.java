package org.refracktory.refrackt.services.techno.parser;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Token;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Tokenizer;
import org.refracktory.refrackt.util.Constants;

public class MathsTokenizer {

    private Log log = Console.console(this);

    public static void main(String[] args) {
        System.out.println(new MathsTokenizer().tokenize("Rosasor Iadul\n" +
                "androgyn human wizard\n" +
                "Humourless\n" +
                "\n" +
                "str:  7\n" +
                "dex:  6\n" +
                "con:  6\n" +
                "int:  7\n" +
                "wis: 10\n" +
                "cha: 11\n" +
                "\n" +
                "ac:   8   armour: none\n" +
                "hp:   4\n" +
                "\n" +
                "equipment: pack: 5 torches, 3 candles, small knife, small sack, hammer\n" +
                "           saddle: mirror, 2+3 days of trail rations, pipe, mess kit, small sack\n" +
                "They carry a written record of the lineage of the king and all of the nobles going back four hundred years. Marking things such as hair color and eye color as it passes down bloodlines\n" +
                "(10+5)*2\n" +
                "(10/5)*3\n")
        );
    }

    public String tokenize(String text) {
        StringBuilder str = new StringBuilder();
        Tokenizer mathsTokenizer = MathsTokenizer.getMathsTokenizer();
        mathsTokenizer.tokenize(text);
        for (Token token : mathsTokenizer.getTokens()) {
            if (token.token == 1) {
                log.debug("math bits : %s", token.sequence);
                str.append(Parser.evalAsText(token.sequence));
            } else {
//                log.info("trash : '%s'" ,token.sequence);
                str.append(token.sequence);
            }
        }
        return str.toString();
    }
    public static Tokenizer getMathsTokenizer() {
        Tokenizer tokenizer = new Tokenizer();
        String REGEX_CHARACTERS = "<([{\\^-=$!|]})?*+.>";
        tokenizer.add("[0-9\\+\\-\\*\\/\\(\\)]+", 1);
        //tokenizer.add("\\b", 2);
//        tokenizer.add("[\\p{Alpha}]+", 99);
        tokenizer.add("[\\p{L}]+", 99);
        // the parens here cause problems with more complex maths:
        // added ; 21/08/21
        tokenizer.add("[:,@'_;\\\"\\.\\?\\!\\[\\]\\}\\{\\(\\)]+", 98);
        tokenizer.add("[\n|\r|\\s]+", Constants.END_OF_LINE); // whitespace and end of lines.
        return tokenizer;
    }

}
