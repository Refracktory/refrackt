package org.refracktory.refrackt.services.techno.composer;

import com.knunk.util.Parser;
import org.refracktory.refrackt.exceptions.GeneratorMissingException;
import org.refracktory.refrackt.exceptions.SelectableMissingException;
import org.refracktory.refrackt.exceptions.TemplateMissingException;
import org.refracktory.refrackt.repository.GeneratorRepository;
import org.refracktory.refrackt.repository.SelectablesRepository;
import org.refracktory.refrackt.repository.TemplateRepository;
import org.refracktory.refrackt.rest.objects.DataRequest;
import org.refracktory.refrackt.services.techno.generation.CommandExecutor;
import org.refracktory.refrackt.services.techno.generation.GenerationContext;
import org.refracktory.refrackt.services.techno.generation.Generator;
import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
import org.refracktory.refrackt.services.techno.selectable.Selected;
import org.refracktory.refrackt.services.techno.tags.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class ComposeService {

  public static final char COMPOSITION_DELIMITER = '~';

  @Autowired
  private SelectablesRepository selectablesRepository;

  @Autowired
  private TemplateRepository templateRepository;

  @Autowired
  private GeneratorRepository generatorRepository;

  @Autowired
  private CommandExecutor commandExecutor;

  public Composition compose(String text, Tags tags) throws SelectableMissingException {
    String[] elements = Parser.splitOutItemsWithin(text, COMPOSITION_DELIMITER, COMPOSITION_DELIMITER);
    for (String element : elements) {
      SelectableItemList selectable = selectablesRepository.get(element);
      Selected result = selectable.select(tags);
      text = text.replace(COMPOSITION_DELIMITER + element + COMPOSITION_DELIMITER, result.getText());
      tags.add(result.getGeneratedTags().toList()); //TODO probably not necessary...
    }
    return new Composition().setComposition(text);
  }

  /**
   * this is for evaluating single terms directly
   * @param text
   * @return
   */
  public String evaluate(String text) throws SelectableMissingException, TemplateMissingException {
    SelectableItemList selectableItemList = selectablesRepository.get(text);
    Selected selected = selectableItemList.select(new Tags());
    return selected.getText();
  }

  public Composition compose(String templateName, DataRequest dataRequest) throws TemplateMissingException, GeneratorMissingException {

    // generation context object
    GenerationContext gc = new GenerationContext(templateName);

    gc.addTags(new Tags(dataRequest.getTags()));
    Properties p = new Properties();
    p.putAll(dataRequest.getProperties());
    gc.addProperties(p);

    Generator generator = generatorRepository.get(templateName);
    generator.compile(commandExecutor, gc);
    //generator.displayLog();
    return generator.compose(gc);
  }
}