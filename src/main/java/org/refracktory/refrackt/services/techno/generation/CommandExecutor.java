package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import com.knunk.util.TextUtility;
import org.refracktory.refrackt.exceptions.DataTableMissingException;
import org.refracktory.refrackt.exceptions.SelectableMissingException;
import org.refracktory.refrackt.repository.DataTableRepository;
import org.refracktory.refrackt.repository.SelectablesRepository;
import org.refracktory.refrackt.services.techno.assigned.AssignTo;
import org.refracktory.refrackt.services.techno.assigned.PicksFrom;
import org.refracktory.refrackt.services.techno.assigned.PicksFromVariable;
import org.refracktory.refrackt.services.techno.datatable.DataTable;
import org.refracktory.refrackt.services.techno.parser.MathsTokenizer;
import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
import org.refracktory.refrackt.services.techno.selectable.Selected;
import org.refracktory.refrackt.services.techno.tags.Assignments;
import org.refracktory.refrackt.services.techno.tags.Tags;
import org.refracktory.refrackt.util.BuildLog;
import org.refracktory.refrackt.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CommandExecutor {

    private Log log = Console.console(this);
    private Set<String> lostVars = new HashSet<>();

    @Autowired
    private SelectablesRepository selectablesRepository;

    @Autowired
    private DataTableRepository dataTableRepository;

    public CommandEvaluation evaluate(Command command, GenerationContext gc) {
        if (command instanceof PicksFrom) {
            return evaluate((PicksFrom) command, gc);
        } else if (command instanceof PicksFromVariable) {
            return evaluate((PicksFromVariable) command, gc);
        } else if (command instanceof AssignTo) {
            return evaluate((AssignTo) command, gc);
//        } else if (command instanceof AssignTo) {
//            return evaluate((AssignTo) command, gc);
        } else if (command instanceof GenerationCommand) {
            return evaluate((GenerationCommand) command, gc);
        }

        log.error("%s not implemented", command.getClass().getName());
        return new CommandEvaluation();
    }

    public CommandEvaluation evaluate(GenerationCommand command, GenerationContext gc) {
        CommandEvaluation ce = new CommandEvaluation();
        command.execute(gc);
        if (GenerationCommandEnum.CLEAR_TAGS.name().equals(gc.getContext())) {
            gc.resetTagsTo(new Tags("nope!!"));
        } else if (GenerationCommandEnum.ADD_TAGS.name().equals(gc.getContext())) {
            gc.addTags(ce.getTags());
        }
        return ce;
    }
    public CommandEvaluation evaluate(AssignTo command, GenerationContext gc) {
        CommandEvaluation ce = compileViaSubstitution(command.getName(), command.getOperation(), gc);
        return ce;
    }

    public CommandEvaluation evaluate(PicksFrom command, GenerationContext gc) {
        String name = command.getName();
        try {
            SelectableItemList si = selectablesRepository.get(command.getSelector().replace("~", ""));
            List<String> items = new ArrayList<>();
            int count = command.getCount();
            for (int i=0; i < count; i++) {
                Selected selected = command.isUnique() ? si.selectAndRemove(gc.getTags()) : si.select(gc.getTags());
                gc.addTags(selected.getGeneratedTags());
                //TODO 'add' items into previous if they are dupes.
                log.info("TODO 'add' items into previous if they are dupes.");
                String selectText = selected.getText();
                // TODO - ONLY WORKS ONCE.
                if (items.indexOf(selectText) > -1) {
                    // this may be overkill:
                    if (selectText.trim().endsWith("@s") || selectText.trim().endsWith("@ves")) {
                        String old = items.remove(items.indexOf(selectText));
                        items.add(Utility.addTwoItems(old, selectText));
                    } else {
                        log.info("singular '%s' item selected as multiple - dropping", selectText);
                    }
                } else {
                  items.add(selected.getText());
                }
            }
            CommandEvaluation ce = compileViaSubstitution(name, items.toString(), gc);
            return ce;
        } catch (SelectableMissingException e) {
            e.printStackTrace();
        }
        CommandEvaluation ce = compileViaSubstitution(command.getName(), command.getOperation(), gc);
        return ce;
    }

    public CommandEvaluation evaluate(PicksFromVariable command, GenerationContext gc) {
        String name = command.getName();
        String cmdtxt = command.getOperation();

        CommandEvaluation ce = compileViaSubstitution(command.getName(), command.getOperation(), gc);
        return ce;
    }

    public CommandEvaluation compileViaSubstitution(String key, String valueText, GenerationContext gc) {
        Tags passed = gc.getTags();
        CommandEvaluation ce = new CommandEvaluation();

        // adding var shortening expansion: ^ -> ~~
        valueText = Utility.expandVariables(valueText);

        // this is the primary substitution pipeline
        String result = valueText;

        // first substitution is done here:
        List<String> vars = Utility.parseOutVariables(valueText);
        result = convertVariables(gc, passed, ce, vars, result);

        result = Utility.expandVariables(result);
        log.info("processing data table lookups - need inside out deconstruction with recursion?");
        List<String> list = parseInsideOutOrder(result, "~");
        for (String varx : list) {
            String var = varx.replace("~", "");
            log.info("processing : '%s'", var);
            try {
                if (result.contains("[") || var.contains("[")) {
                    result = processDataTable(gc, result, var);
                } else if (dataTableRepository.contains(var)) {
                    log.info("auto table lookup of '%s'", var);
                    DataTable dt = dataTableRepository.get(var);
                    // check for default index
                    if (dt.hasReferencedValues()) {
                        if (gc.getProperties().containsKey(dt.getReferencedBy())) {
                            String refed = gc.getProperties().get(dt.getReferencedBy());
                            log.info("setting '%s' to '%s'", var, refed);
                            result = result.replace(varx, dt.find(dt.getName(), refed));
                        } else {
                            log.error("referenced value '%s' could not be located in current properties", var);
                        }
                    } else {
                        log.warn("ummmm '%s' not handled yet, or maybe ever. check 'referenced by' value", var);
                    }
                }
            } catch (DataTableMissingException dtme) {
                log.error("*" + dtme.getMessage());
            }
        }
        // need to process this data table style: armour[plate][ac]
        if (!result.startsWith("[") && result.contains("[")) {
            log.info("processing direct data table lookups:");
            try {
                result = processDataTable(gc, result);
            } catch (DataTableMissingException e) {
                e.printStackTrace();
            }
        }

        result = Utility.doFunctions(result);
        result = TextUtility.replaceEnglishStuff(result);
        result = doCalculationOn(result);
        log.info("result : %s", result);
        ce.add(key, result);
//        log.info("final: '%s'", result);
        return ce;
    }

    /**
     * This does not recurse nor return inside out order. Placeholder.
     * @param text
     * @param delimiter
     * @return
     */
    public static List<String> parseInsideOutOrder(String text, String delimiter) {
//        List<String> list = Arrays.asList(text.split(delimiter));
        List<String> list = Utility.parseOutVariables(text, delimiter.toCharArray()[0]);
        return list.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
        //        List<String> list = new ArrayList<>();
//        // hard work here:
//        int count = count(text, delimiter);
//        if (count % 2 == 1) {
//            throw new IllegalArgumentException(String.format("poorly formed text with %d delimiters", count));
//        }
//        int middle = count / 2;
//        middle--;
//        return list;
    }

    public static int count(final String text, final String which)
    {
        //FIXME THIS IS STUPID
        if (which.length() > 1)
        {
            return -1;
        }
        int count = 0;
        for (int i = 0; i < text.length(); i++)
        {
            if (text.substring(i, i + 1).equals(which))
            {
                count++;
            }
        }
        return count;
    }

    public static int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }


    private String convertVariables(GenerationContext gc, Tags passed, CommandEvaluation ce, List<String> vars, String text) {
        for (String var : vars) {
            log.info("processing convert : '%s'", var);
            try {
                // first, need to look and see if we have the value already
                if (gc.getTags().contains(var)) {
               // TODO THIS NEEDS CODED BETTER WITH TRUE/FALSE TESTING
                    text = text.replaceFirst(Utility.raise(var), "true");
                } else if (gc.getProperties().containsKey(var)) {
                    String actual = gc.getProperties().get(var);
                    if (actual.contains("^")) {
                        actual = Utility.expandVariables(actual);
                    }
                    text = text.replaceFirst(Utility.raise(var), actual);
                    log.info("retrieved '%s' from property list as '%s'", var, actual);
                } else if (var.contains("@")) {
                    if (var.equals("@tags")) {
                        text = gc.getTags().toMiniString();
                        log.info("set tags to %s", text);
                    } else {
                        log.error("don't know how to process '%s'", var);
                    }
                } else if (!var.contains("[")) {
                    SelectableItemList si = selectablesRepository.get(var);
                    String actual = "";
                    if (si == null) {
                        // lostVars added to control constant retries for missing selectors. You can miss once.
                        // this may mess stuff up.
                        if (!lostVars.contains(var)) {
                            lostVars.add(var);
                            String msg = String.format("could not locate '%s' selector", var);
                            BuildLog.getBuildLog().addError(msg);
                            log.info(msg);
                            // put back into system and hope for the best. (late binding)
                            actual = Utility.raise(var);
                        } else {
                            String msg = String.format("could not locate lost selector : '%s'", var);
                            actual = var;
                        }

                    } else {
                        Selected selected = si.select(passed);
                        actual = selected.getText();
                        log.info("selected '%s' as '%s'", var, actual);

                        if (selected.getGeneratedTags().exist()) {
                            log.info("adding generated tags '%s'", selected.getGeneratedTags());
                            ce.add(selected.getGeneratedTags());
                            gc.addTags(selected.getGeneratedTags());
                        }
                        if (selected.getGeneratedTags().getAssignments().exist()) {
                            Assignments assignments = selected.getGeneratedTags().getAssignments();
                            ce.addAll(assignments.getAssignmentProperties());
                            gc.addProperties(assignments.getAssignmentProperties());
                        }

                        if (actual.contains("^")) {
                            actual = Utility.expandVariables(actual);
                            actual = convertVariables(gc, passed, ce, Utility.parseOutVariables(actual), actual);
                        }
                    }
                    text = text.replaceFirst(Utility.raise(var), actual);
                }
                log.info("<%s>", text);
            } catch (SelectableMissingException sme) {
                log.error("*" + var + "* : " + sme.getMessage());
//                log.error("store exception to generation log...");
                // TODO TRUE/FALSE coding for algorithms???
                text = text.replaceFirst(Utility.raise(var), "false");
            }
        }
        if (text.contains("~")) {
            List<String> newvars = Utility.parseOutVariables(text);
            text = convertVariables(gc, passed, ce, newvars, text);
        }
        return text;
    }

    public String processDataTable(GenerationContext gc, String result) throws DataTableMissingException {
        String lookup = result.substring(0, result.indexOf("["));
        DataTable dataTable = dataTableRepository.get(lookup);
        String index = Parser.tweenParse(result, "[", "]");
        String attribute = result.substring(result.lastIndexOf("[")+1, result.lastIndexOf("]"));
        if (attribute.equals(index)) {
            String val = dataTable.find(lookup, index);
            log.info("%s[%s][%s] tabled out: '%s'", lookup, index, attribute, val);
            return val;
        } else {
            String val = dataTable.find(index, attribute);
            log.info("%s[%s][%s] tabled out: '%s'", lookup, index, attribute, val);
            return val;
        }
    }

    public String processDataTable(GenerationContext gc, String result, String var) throws DataTableMissingException {
        log.info("data table lookup!");
        String lookup = var.substring(0, var.indexOf("["));
        DataTable dataTable = dataTableRepository.get(lookup);
        String argument = Parser.tweenParse(var, "[", "]");
        // FIXME BIG ONE!! ?
        if (dataTable.hasReferencedValues()) {
            String index = dataTable.getReferencedBy();
            String value = dataTable.find(gc.getProperties().get(index), "?");
            result = result.replace(Utility.raise(var), value);
        } else if (argument.trim().length() > 0) {
            String value = dataTable.find(argument, "?");
            result = result.replace(Utility.raise(argument), value);
        }
        else {
            log.error("i don't know what else to do!!");
        }
        log.info(dataTable.toString());
        return result;
    }

    private String doCalculationOn(String values) {
        return new MathsTokenizer().tokenize(values);
    }

}
