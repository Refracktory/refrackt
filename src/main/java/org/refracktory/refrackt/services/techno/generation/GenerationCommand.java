package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.Parser;
import org.refracktory.refrackt.services.techno.tags.Tags;

public class GenerationCommand extends Command {
    public GenerationCommand(String text) {
        super();
        setActualText(text);
        // good for now?
        setCompiledText(text);
    }

    @Override
    public void execute(GenerationContext gc) {
        if (getCompiledText().startsWith("@cleartags(")) {
            log.info("clear specified tags if they were previously generated in the gc");
            String tagList = Parser.tweenParse(getCompiledText(), "(", ")");
            String[] tags = tagList.split(",");
            for (String tag : tags) {
                if (gc.getTags().remove(tag.trim())) {
                    log.info("removed tag '%s'", tag.trim());
                } else {
//                    log.info("not tagged with'%s'", tag.trim());
                }
            }
            gc.setContext(GenerationCommandEnum.CLEAR_SPECIFIC_TAGS.name());
        } else if (getCompiledText().equals("@cleartags")) {
            log.info("clear all tags that were previously generated in the gc");
            gc.clearTags();
            gc.setContext(GenerationCommandEnum.CLEAR_TAGS.name());
        } else if (getCompiledText().startsWith("@tags(")) {
            String tagList = Parser.tweenParse(getCompiledText(), "(", ")");
            log.info("adding tags to generation context: %s", tagList);
            String[] tags = tagList.split(",");
            for (String tag : tags) {
                gc.addTags(new Tags(tagList));
            }
            gc.setContext(GenerationCommandEnum.ADD_TAGS.name());
        } else {
            log.error("found a generation command that can't be executed: '%s'", getActualText());
        }

//        if (GenerationCommandEnum.CLEAR_SPECIFIC_TAGS == ce.getStatus()) {
//            log.warn("you were supposed to clear some!!");
//        } else if (GenerationCommandEnum.CLEAR_TAGS == ce.getStatus()) {
//            gc.clearTags();
//            ce.getTags().clear();
//            gc.addTags(g.getStartingTags());
//        } else {
//            log.info("set these tags into gc %s", ce.getTags());
//            gc.addTags(ce.getTags());
//        }
        log.warn("we aren't, really");
        gc.setContext(GenerationCommandEnum.WERE_GOOD.name());
    }
}
