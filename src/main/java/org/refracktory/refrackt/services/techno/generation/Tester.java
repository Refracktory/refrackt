package org.refracktory.refrackt.services.techno.generation;

import org.refracktory.refrackt.services.techno.files.FileManager;
import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
import org.refracktory.refrackt.services.techno.selectable.SelectableTokenizer;
import org.refracktory.refrackt.services.techno.selectable.Selected;
import org.refracktory.refrackt.services.techno.tags.Tags;
import org.refracktory.refrackt.templates.Template;

public class Tester {
    public static void main(String[] args) {
        new Tester().test();
    }
    public void test() {

        Template template = new Template("test", "dex assigned to ~3d6~");
        Generation generation = new Generation(template);

        Tags tags = new Tags();
        tags.add("sad");

        GenerationSet g1 = generation.startGeneration(tags);
        g1.add(getRace(tags));
        System.out.println(g1);

        GenerationSet g2 = g1.next();
        g2.add(getColor(tags));
        System.out.println(g2);

        // revert one version
//        System.out.println("g :" + g);
//        System.out.println("gp:" + g.previous());

        GenerationSet g3 = g2.next();
        g3.add(getAdjective(tags));
        g3.add(getMotivator(tags));
        g3.add(selectFromFile(tags, "files/texture.sel"));

        //FIXME: load'em all
        //nxt.add(getFromFile(t, "files/color.catalog"));
        System.out.println(g3);

        GenerationSet g4 = g3.next();
        g4.add(getClass(tags));
        g4.add(selectFromFile(tags, "files/construct.sel"));

        System.out.println(g4);

        // 2023-06-10 this is left to make work:
        generation.compile();
        System.out.println(generation.getComposition());

        System.out.println("g :" + generation);

    }

    public Selected selectFromFile(Tags tags, String selectorFileName) {
        if (selectorFileName.endsWith(".catalog")) throw new IllegalArgumentException("we cannot parse .catalog files with this method");
        String text = FileManager.getSelectorFromFile(selectorFileName).getText();
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        Selected selected = list.select(tags);
        return selected;
    }

    public Selected getColor(Tags tags) {
        String text = "color selects from\n" +
                " red=1[red]\n" +
                " purple=1[purple]\n" +
                " blue=1[blue]\n" +
                " green=1[green]\n" +
                " yellow=1[yellow]\n" +
                " white=1[white]\n" +
                " black=1[black]\n";
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        Selected selected = list.select(tags);
        return selected;
    }

    public Selected getAdjective(Tags tags) {
        String text = "mystical-adjective selects from\n" +
                " glowing=1{red}\n" +
                " resonating=1{red}\n" +
                " humming=1{purple}\n" +
                " luminous=1{blue|divine}[aging=+1]\n" +
                " radiant=1{divine|white}[spiritual,aging=+2]\n" +
                " pulsing=1{green}\n" +
                " chiming=1{happy&divine}\n" +
                " whining=1{yellow}\n" +
                " wailing=1{sad|divine}[supernatural,aging=+3]\n" +
                " keening=1{sad&!divine}[supernatural]\n" +
                " moaning=1{white}";
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        Selected selected = list.select(tags);
        return selected;
    }

    public Selected getMotivator(Tags tags) {
        String text = "primary-motivator selects from\n" +
                " Achievement=5\n" +
                " Acquisition=5\n" +
                " Balance/Peace=5{neutral}\n" +
                " Beneficence=5{good}\n" +
                " Chaos=1{chaotic}\n" +
                " Creation=5\n" +
                " Destruction=1{chaotic&evil}\n" +
                " Discovery/Adventure=5{supernatural}\n" +
                " Education=5\n" +
                " Enslavement=10{evil}\n" +
                " Hedonism=10{evil}\n" +
                " Liberation=5{lawful}\n" +
                " Nobility/Honor=10{lawful}\n" +
                " Order=5{lawful}\n" +
                " Play=10\n" +
                " Power=5\n" +
                " Recognition=5\n" +
                " Service=5{spiritual}\n" +
                " Torment=10{evil}\n" +
                " Understanding=10";
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        Selected selected = list.select(tags);
        return selected;
    }

    public Selected getClass(Tags tags) {
        String text = "class selects from\n" +
                " Wizard=5[wizard,smart,wise]\n" +
                " Cleric=10[cleric,wise,charismatic]\n" +
                " Ranger=5{good}[fighter,tough,dexterous]\n" +
                " Fighter=25[fighter,strong,dexterous]\n" +
                " Paladin=5{lawful&good}[paladin,charismatic,charming,strong]\n" +
                " Rogue=10{!good}[rogue,dexterous,wise,agile,charming]";
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        Selected selected = list.select(tags);
        return selected;
    }

    public Selected getRace(Tags tags) {
        String text = "race selects from\n" +
        " Human=90[human,racial-stock+=human]\n" +
                " Wood Elf=10[elf,racial-stock+=woodelf]\n" +
                " High Elf=5[elf,racial-stock+=highelf]\n" +
                " Mountain Dwarf=10[dwarf,racial-stock+=mountaindwarf]\n" +
                " Hill Dwarf=10[dwarf,racial-stock+=hilldwarf]\n" +
                " Stout Hobbit=1[hobbit,racial-stock+=stouthobbit]\n" +
                " Tallfellow Hobbit=1[hobbit,racial-stock+=tallfellowhobbit]";
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        Selected selected = list.select(tags);
        return selected;
    }

    public Selected getWeapon(Tags tags) {
        String text = "weapon selects from\n" +
                " sword=10[edged_weapon,damage=+1]\n" +
                " mace=10[bludgeon_weapon,heavy,damage=+1]\n" +
                " axe=10[bludgeon_weapon,damage=+4]";
        SelectableItemList list = SelectableTokenizer.selectFrom(text);
        Selected selected = list.select(tags);
        return selected;
    }
}
