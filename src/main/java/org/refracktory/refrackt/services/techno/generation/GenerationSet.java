package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.AbstractPersistentProperties;
import com.knunk.util.Console;
import com.knunk.util.Log;
import org.refracktory.refrackt.services.techno.selectable.Selected;
import org.refracktory.refrackt.services.techno.tags.Tags;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class GenerationSet {
    transient private Log log = Console.console(this);
    private final int iteration;
    private Tags startingTags = new Tags();
    private Tags tags = new Tags();
    private List<Selected> selected = new ArrayList<>();
    private AbstractPersistentProperties properties = new AbstractPersistentProperties();
    private Generation generation;

    protected GenerationSet(Generation g, Tags startingTags, int iteration) {
        this.generation = g;
        this.startingTags.add(startingTags.toList());
        this.tags.add(startingTags.toList());
        this.iteration = iteration;
        log.info("created generation set");
    }

    public void resetTags() {
        this.tags = new Tags(generation.getStartingTags());
    }

    public void add(Selected select) {
        properties.setProperty(select.getName(), select.getText());
        selected.add(select);
        Tags assigned = select.getGeneratedTags();

        tags.add(assigned);
    }

    public Tags getTags() {
        return tags;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("GenerationSet %2d starting %s\n", iteration, startingTags.toMiniString()));
        for (Selected selected : selected) {
            sb.append(selected);
            sb.append("\n");
        }
        sb.append(String.format("GenerationSet %2d ending %s\n", iteration, tags.toMiniString()));
        return sb.toString();
    }

    public GenerationSet next() {
        GenerationSet gnext = generation.next(tags);
        return gnext;
    }

    public void add(CommandEvaluation ce) {
        log.info("do nothing...");
//        properties.putAll(ce.getProperties());
//        tags.add(ce.getTags());
    }

    public Properties getProperties() {
        return properties.getProperties();
    }

    public void addProperties(Properties properties) {
        this.properties.putAll(properties);
    }
}
