package org.refracktory.refrackt.services.techno.selectable;

import org.refracktory.refrackt.services.techno.tags.Tags;

public class SelectableItemResult {
  public String getItem() {
    return item;
  }

  public Tags getTags() {
    return tags;
  }

  private String item;
  private Tags tags;

  public SelectableItemResult(String item, Tags tags) {
    this.item = item;
    this.tags = tags;
  }
}
