package org.refracktory.refrackt.services.techno.generation;

public enum GenerationCommandEnum {
    CLEAR_TAGS, ADD_TAGS, CLEAR_SPECIFIC_TAGS, WERE_GOOD;
}
