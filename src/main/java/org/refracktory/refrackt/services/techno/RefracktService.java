package org.refracktory.refrackt.services.techno;

import org.refracktory.refrackt.exceptions.GeneratorMissingException;
import org.refracktory.refrackt.exceptions.SelectableMissingException;
import org.refracktory.refrackt.exceptions.TemplateMissingException;
import org.refracktory.refrackt.repository.SelectablesRepository;
import org.refracktory.refrackt.repository.TemplateRepository;
import org.refracktory.refrackt.rest.objects.DataRequest;
import org.refracktory.refrackt.services.techno.composer.ComposeService;
import org.refracktory.refrackt.services.techno.composer.Composition;
import org.refracktory.refrackt.services.techno.selectable.SelectableItem;
import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
import org.refracktory.refrackt.services.techno.tags.Tags;
import org.refracktory.refrackt.templates.Template;
import org.refracktory.refrackt.util.BuildLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RefracktService {

  @Autowired
  private ComposeService composeService;

  @Autowired
  private TemplateRepository templateRepository;

  @Autowired
  private SelectablesRepository selectablesRepository;

  public String getTemplate(String term) throws SelectableMissingException, TemplateMissingException {
      return composeService.evaluate(term);
  }

  public Template getTemplate(String pageName, String id) throws TemplateMissingException {
      Template template = templateRepository.get(pageName);
      return template;
  }

    public Composition generate(String pageName, String id, DataRequest dataRequest) throws TemplateMissingException, GeneratorMissingException, SelectableMissingException {
        Composition composition = composeService.compose(pageName, dataRequest);
        BuildLog.getBuildLog().messages();
        return composition;
    }

    public List<String> getListOfSelectors() {
      return selectablesRepository.keySet();
    }

    public List<SelectableItem> getDataForSelector(String selector) throws SelectableMissingException {
      try {
          SelectableItemList list = selectablesRepository.get(selector);
          return list.all();
      } catch (SelectableMissingException sme) {
          List<SelectableItem> list = new ArrayList<>();
          return list;
      }
    }

    public List<SelectableItem> getDataForSelector(String selector, Tags tags) {
        try {
            SelectableItemList list = selectablesRepository.get(selector);
            return list.apply(tags);
        } catch (SelectableMissingException sme) {
            List<SelectableItem> list = new ArrayList<>();
            return list;
        }
    }

    public String getASelectorResult(String selector, Tags tags) throws SelectableMissingException {
      return composeService.compose(selector, tags).getText();
    }
}
