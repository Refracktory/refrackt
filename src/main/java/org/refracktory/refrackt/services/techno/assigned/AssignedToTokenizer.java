package org.refracktory.refrackt.services.techno.assigned;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import org.refracktory.refrackt.exceptions.SelectableMissingException;
import org.refracktory.refrackt.repository.SelectablesRepository;
import org.refracktory.refrackt.services.techno.generation.GeneratorTokenizer;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Token;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Tokenizer;
import org.refracktory.refrackt.services.techno.selectable.SelectableItemList;
import org.refracktory.refrackt.util.Constants;
import org.refracktory.refrackt.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.util.LinkedList;

@Configurable
public class AssignedToTokenizer {
    private Log log = Console.console(this);

    public SelectablesRepository selectablesRepository;

    public AssignedToTokenizer(@Autowired SelectablesRepository selectablesRepository) {
        this.selectablesRepository = selectablesRepository;
    }

    public AssignTo parse(String text) {
        Tokenizer tokenizer = getAssignedToTokenizer();
        tokenizer.tokenize(text);
        LinkedList<Token> list =  tokenizer.getTokens();

        String var = null;
        String operation = null;
        String dataBasedOn = null;

        for (Token token: list) {
            if (token.token == 0) {
                String[] pair = token.sequence.split(" assigned to ");
                var = pair[0];
                operation = pair[1];
                log.info("assigning '%s' to '%s'", var, operation);
                AssignTo assignTo = new AssignTo(var, operation);
                assignTo.setActualText(text);
                return assignTo;
            } else if (token.token == 1) {
                String[] pair = token.sequence.split(" calculated from ");
                var = pair[0];
                if (pair[0].contains("[")) {
                    var = var.substring(var.indexOf("["));
                    dataBasedOn = Parser.tweenParse(pair[0], "[", "]");
                }
                operation = pair[1];
                log.info("calculating '%s' from '%s'", var, operation);
                AssignToCalculation assignTo = new AssignToCalculation(var, dataBasedOn, operation);
                assignTo.setActualText(text);
                return assignTo;
            } else if (token.token == GeneratorTokenizer.PICKS_FROM || token.token == GeneratorTokenizer.PICKS_UNIQUE_FROM) {
                // pack picks 5 from ~pack-items~
                // pack picks 5 unique from ~pack-items~
                String toktxt = token.sequence;
                var = toktxt.substring(0, toktxt.indexOf(" "));
                operation = toktxt.substring(toktxt.indexOf(" ") + 1);
                String countString = "0";
                boolean unique = false;
                String subsStr = Parser.tweenParse(toktxt, "picks ", " from");

               if (subsStr.contains("unique")) {
                    unique = true;
                    countString = Parser.tweenParse(toktxt, "picks ", " unique from");
                } else {
                    countString = Parser.tweenParse(toktxt, "picks ", " from");
                }
                int count = 1;
                if (countString.contains("~")) {
                    try {
                        String diceVar = Parser.tweenParse(countString,"~", "~");
                        SelectableItemList si = selectablesRepository.get(diceVar);
                        count = Integer.parseInt(si.select().getText());
                        countString = countString.replace(Utility.raise(diceVar), String.valueOf(count));
                        countString = Parser.simplifyTextMaths(countString);
                        count = Integer.parseInt(countString);
                    } catch (SelectableMissingException e) {
                        e.printStackTrace();
                    }
                } else {
                    count = Integer.parseInt(countString);
                }
                String selector = toktxt.substring(toktxt.lastIndexOf(" from ") + 6);
                PicksFrom picksFrom = new PicksFrom(var, operation, count, unique, selector);
                picksFrom.setActualText(toktxt);
                return picksFrom;
            } else if (token.token == GeneratorTokenizer.PICKS_FROM_VARIABLE
                    || token.token == GeneratorTokenizer.PICKS_UNIQUE_FROM_VARIABLE) {
                // pack picks ~count~ from ~pack-items~
                String toktxt = token.sequence;
                var = toktxt.substring(0, toktxt.indexOf(" "));
                operation = toktxt.substring(toktxt.indexOf(" ") + 1);
                PicksFromVariable picksFrom = new PicksFromVariable(var, operation);
                picksFrom.setActualText(toktxt);
                return picksFrom;
            } else if (token.token == 13) {
                //tsilb
            }
        }
//        String tokenized = tokenizer.detokenize();
//        log.info("\nsucceeded = " + tokenized.equals(text));
        // should never get here.
        AssignTo assignTo = new AssignTo("void", "no-op");
        assignTo.setActualText(text);
        return assignTo;
    }

    public static Tokenizer getAssignedToTokenizer() {
        Tokenizer tokenizer = new Tokenizer();
        //~random_hp~+hp_bonus[~con~]
        tokenizer.add("([a-zA-Z0-9_~\\-]+) assigned to [a-zA-Z0-9_~@\\*\\+\\{\\}\\|\\-\\^]+", 0);
        tokenizer.add("([a-zA-Z0-9_~\\-]+) calculated from [a-zA-Z0-9_~@\\,\\{\\}\\-\\[\\]\\\\+\\*\\/]+", 1);
        tokenizer.add("([a-zA-Z0-9_\\-]+) picks [0-9~a-zA-Z\\+\\-]+ from [a-zA-Z0-9_~\\*\\-\\^\\|@\\{\\}]+", GeneratorTokenizer.PICKS_FROM);
        tokenizer.add("([a-zA-Z0-9_\\-]+) picks [0-9~a-zA-Z\\+\\-]+ unique from [a-zA-Z0-9_~\\*\\-\\^\\|@\\{\\}]+", GeneratorTokenizer.PICKS_UNIQUE_FROM);
        tokenizer.add("([a-zA-Z0-9_\\-]+) picks [a-zA-Z0-9_~\\*\\-]+ from [a-zA-Z0-9_~\\*\\-\\^\\|@\\{\\}]+", GeneratorTokenizer.PICKS_FROM_VARIABLE);
        tokenizer.add("([a-zA-Z0-9_\\-]+) picks [a-zA-Z0-9_~\\*\\-]+ unique from [a-zA-Z0-9_~\\*\\-\\^\\|@\\{\\}]+", GeneratorTokenizer.PICKS_UNIQUE_FROM_VARIABLE);
        tokenizer.add("^#.*", Constants.COMMENT_LINE); // skip line
        tokenizer.add("[\n|\r]", Constants.END_OF_LINE); // whitespace and end of lines.
        return tokenizer;
    }

//    public static void main(String[] args) {
//        testSelect();
//    }

//    private static void testSelect() {
//        AssignTo assignTo = new AssignedToTokenizer().parse("dex assigned to ~3d6~\n");
//    }

}
