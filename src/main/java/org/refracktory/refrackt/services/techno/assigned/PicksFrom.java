package org.refracktory.refrackt.services.techno.assigned;

import org.refracktory.refrackt.services.techno.generation.GenerationContext;

public class PicksFrom extends AssignTo {

    private final int count;

    private final String selector;
    private final boolean unique;

    public PicksFrom(String name, String operation, int count, boolean unique, String selector) {
        super(name, operation);
        this.count = count;
        this.unique = unique;
        this.selector = selector;
    }

    @Override
    public void execute(GenerationContext gc) {
        log.info("?");
    }

    public int getCount() {
        return count;
    }

    public String getSelector() {
        return selector;
    }

    public boolean isUnique() {
        return unique;
    }
}
