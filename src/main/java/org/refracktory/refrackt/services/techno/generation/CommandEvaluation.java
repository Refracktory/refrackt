package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.Console;
import com.knunk.util.Log;
import org.refracktory.refrackt.services.techno.tags.Tags;

import java.util.Properties;

public class CommandEvaluation {
    private Log log = Console.console(this);
    private final Tags tags = new Tags();
    private final Properties properties = new Properties();
    private GenerationCommandEnum status = GenerationCommandEnum.WERE_GOOD;

    public void setResults(String results) {
        this.results = results;
    }

    public String getResults() {
        return results;
    }

    private String results;

    public CommandEvaluation() {
        super();
    }

    public Tags getTags() {
        return tags;
    }

    public void add(Tags generatedTags) {
        tags.add(generatedTags);
    }

    public void add(String key, String value) {
        log.info("setting %s to '%s'", key, value);
        properties.put(key, value);
    }

    public Properties getProperties() {
        return properties;
    }

    public void addAll(Properties assignmentProperties) {
        properties.putAll(assignmentProperties);
    }

    public GenerationCommandEnum getStatus() {
        return GenerationCommandEnum.WERE_GOOD;
    }

    public void setStatus(GenerationCommandEnum status) {
        this.status = status;
    }
}
