package org.refracktory.refrackt.services.techno.criteria;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import org.refracktory.refrackt.services.techno.tags.Tags;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Criteria {
  public static final String CONDITIONAL_REGEX = "<=|>=|<>|!=|\\=\\=|\\=|<|>";
  public static final String LOGICAL_REGEX = "\\||&|!";

  public final static String LEFT_DELIMITER = "{";
  public final static int LEFT_DELIMITER_SIZE = LEFT_DELIMITER.length();

  public final static String RIGHT_DELIMITER = "}";
  public final static int RIGHT_DELIMITER_SIZE = RIGHT_DELIMITER.length();
  private final String text;
  private Log logger = Console.console(this);

  public Criteria(String criteria) {
    this.text = criteria;
  }

  public boolean match(Tags tags) {
    String tmp = text.replaceAll("\\(|\\)", "");
    String[] onlyTags = tmp.split(CONDITIONAL_REGEX + "|" + LOGICAL_REGEX);
    Arrays.sort(onlyTags);
    List<String> list = Arrays.asList(onlyTags);
    Collections.reverse(list);
    String current = text;
    for (String tag : tags.toList()) {
        //logger.info("replacing %s", tag);
        if (!tag.isEmpty()) {
          current = current.replace(tag, "true");
          current = current.replace(tag.toLowerCase(), "true");
        }

    }
    // this is here to 'false-out' any remaining tags. AFAIK not actually necessary due to how isMostlyTrue works
    for (String tag : list) {
      //logger.info("xplacing %s", tag);
      if (!tag.isEmpty()) {
        current = current.replace(tag, "false");
//        current = current.replace(tag.toLowerCase(), "false");
      }
    }
    //logger.info(current);
    return Parser.isMostlyTrue(current);
  }

  private boolean evaluate(Tags tags) {
    return false;
  }

  @Override public String toString() {
    return "Criteria{" +
      "text='" + text + '\'' +
      '}';
  }
}
