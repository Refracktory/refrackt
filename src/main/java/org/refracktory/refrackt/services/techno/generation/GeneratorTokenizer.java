package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.Console;
import com.knunk.util.Log;
import org.refracktory.refrackt.repository.SelectablesRepository;
import org.refracktory.refrackt.services.techno.assigned.AssignTo;
import org.refracktory.refrackt.services.techno.assigned.AssignedToTokenizer;
import org.refracktory.refrackt.services.techno.datatable.DataTable;
import org.refracktory.refrackt.services.techno.datatable.DataTableTokenizer;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Token;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Tokenizer;
import org.refracktory.refrackt.util.Constants;
import org.refracktory.refrackt.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;

@Component
public class GeneratorTokenizer {
    public static final int ASSIGN_TO = 0;

    public static final int PICKS_FROM = 10;
    public static final int PICKS_UNIQUE_FROM=11;
    public static final int PICKS_FROM_VARIABLE = 20;
    public static final int PICKS_UNIQUE_FROM_VARIABLE=21;

    public static final int DATA_ARRAY = 1;
    public static final int CALCULATION = 2;
    public static final int SHORTCUT_ASSIGN_TO = 3;
    private static final int GENERATION_COMMAND = 4;

    @Autowired
    public SelectablesRepository selectablesRepository;

    private static Log log = Console.console(GeneratorTokenizer.class);
    private Tokenizer tokenizer;

    // constructor used for testing:
    public GeneratorTokenizer() {
    }

    public void doTokenizing(String text) {
        tokenizer = getGeneratorTokenizer();
        tokenizer.tokenize(text);
        LinkedList<Token> list =  tokenizer.getTokens();

        for (Token token: list) {
            log.info("  ¬[%2d]:%s", token.token, token.sequence.replace("\n", ""));
            if (token.token == ASSIGN_TO) {
                log.info("assignment :" + token.sequence);
            } else if (token.token == DATA_ARRAY) {
                log.info("arrayed line");
            } else if (token.token == PICKS_FROM || token.token == PICKS_UNIQUE_FROM || token.token == PICKS_FROM_VARIABLE || token.token == PICKS_UNIQUE_FROM_VARIABLE) {
                log.info("picks from");
            } else if (token.token == Constants.COMMENT_LINE) {
                log.info("commented line");
            } else if (token.token == Constants.END_OF_LINE) {
                log.info("eol");
            }
        }
        String tokenized = tokenizer.detokenize();
        log.info("\nsucceeded = " + tokenized.equals(text));
    }

    public Generator generateFrom(String name, String fileContents) {
        Generator g = new Generator(name, fileContents);
        GeneratorTokenizer generatorTokenizer = new GeneratorTokenizer();
        generatorTokenizer.doTokenizing(fileContents);
        AssignedToTokenizer assignedToTokenizer = new AssignedToTokenizer(selectablesRepository);
        for (Token token: generatorTokenizer.tokenizer.getTokens()) {
            log.info("  ¬[%2d]:%s", token.token, token.sequence.replace("\n", ""));
            if (token.token == ASSIGN_TO) {
                log.info("assignment :" + token.sequence);
                g.add(assignedToTokenizer.parse(token.sequence));
            } else if (token.token == SHORTCUT_ASSIGN_TO) {
                    log.info("shortcut assignment :" + token.sequence);
                    String var = token.sequence;
                    g.add(assignedToTokenizer.parse(Utility.deraise(var) + " assigned to " + var));
            } else if (token.token == PICKS_FROM || token.token == PICKS_UNIQUE_FROM) {
                g.add(assignedToTokenizer.parse(token.sequence));
            } else if (token.token == PICKS_FROM_VARIABLE || token.token == PICKS_UNIQUE_FROM_VARIABLE) {
                g.add(assignedToTokenizer.parse(token.sequence));
            } else if (token.token == DATA_ARRAY) {
                DataTable dt = DataTableTokenizer.dataizeFrom(token.sequence);
                log.info("array is %s", dt);
                // eventually, should be added to repo
                g.add(dt);
            } else if (token.token == CALCULATION) {
                log.info("assignment with calc: " + token.sequence);
                AssignTo at = assignedToTokenizer.parse(token.sequence);
                g.add(at);
            } else if (token.token == GENERATION_COMMAND) {
                log.info("found a generation command: " + token.sequence);
                g.add(new GenerationCommand(token.sequence));
            } else if (token.token == Constants.COMMENT_LINE) {
                log.info("commented line");
            } else if (token.token == Constants.END_OF_LINE) {
                log.info("eol");
            }
        }

        return g;
    }

    public Tokenizer getGeneratorTokenizer() {
        Tokenizer tokenizer = new Tokenizer();

        tokenizer.add("([a-zA-Z0-9_\\-]+) assigned to [a-zA-Z0-9_~\\*\\-\\+\\^\\|@\\{\\}]+", ASSIGN_TO);
        tokenizer.add("([a-zA-Z0-9_\\-]+) picks [0-9~a-zA-Z\\+\\-]+ from [a-zA-Z0-9_~\\*\\-\\^\\|@\\{\\}]+", PICKS_FROM);
        tokenizer.add("([a-zA-Z0-9_\\-]+) picks [0-9~a-zA-Z\\+\\-]+ unique from [a-zA-Z0-9_~\\*\\-\\^\\|@\\{\\}]+", PICKS_UNIQUE_FROM);
        tokenizer.add("([a-zA-Z0-9_\\-]+\\[[a-zA-Z0-9_\\-]+\\]) array is ([a-zA-Z0-9_~,\\(\\)\\-\\+ ]+)", DATA_ARRAY);
        tokenizer.add("([a-zA-Z0-9_\\-]+) calculated from [a-zA-Z0-9_~@\\{\\}\\,\\+\\-\\]\\[]+", CALCULATION);
        tokenizer.add("^@.*", GENERATION_COMMAND);
        tokenizer.add("^[~].*[~]", SHORTCUT_ASSIGN_TO);
        tokenizer.add("^#.*", Constants.COMMENT_LINE); // skip line
        tokenizer.add("[\n|\r]", Constants.END_OF_LINE); // whitespace and end of lines.
        return tokenizer;
    }

    public static void main(String[] args) {
        testSelect();
    }

    private static void testSelect() {
        new GeneratorTokenizer().doTokenizing("hp_bonus[con] array is (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20) values(-5,-4,-4,-3,-3,-2,-2,-1,-1,0,0,0,0,1,1,2,2,3,3,4,4,5)\n");
    }

}
