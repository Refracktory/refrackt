package org.refracktory.refrackt.services.techno.todo;

public class SubSelectSelector {
    /**
     * mf selects from ae=1,ael=1,aer=1,aias=1,ah=1,aith=1,al=1,ali=1,am=1,an=1,ar=1,ari=1,aro=1,as=1,ath=1,avel=1,brar=1,dar=1,deth=1,dre=1,drim=1,dul=1,ean=1,el=1,emar=1,en=1,er=1,ess=1,evar=1,fel=1,hal=1,har=1,hel=1,ian=1,iat=1,ik=1,il=1,im=1,in=1,ir=1,is=1,ith=1,kash=1,ki=1,lan=1,lam=1,lar=1,las=1,lian=1,lis=1,lon=1,lyn=1,mah=1,mil=1,mus=1,nal=1,nes=1,nin=1,nis=1,on=1,or=1,oth=1,que=1,quis=1,rah=1,rad=1,rail=1,ran=1,reth=1,ro=1,ruil=1,sal=1,san=1,sar=1,sel=1,sha=1,spar=1,tae=1,tas=1,ten=1,thal=1,thar=1,ther=1,thi=1,thus=1,ti=1,tril=1,ual=1,uath=1,us=1,van=1,var=1,vain=1,via=1,vin=1,wyn=1,ya=1,yr=1,yth=1,zair=1
     *
     * v subselects(^[a,e,i,o,u]) from mf
     * c subselects(^[^a,e,i,o,u]) from mf
     * w subselects(^[a,e,i,o,u]) from mf
     * d subselects(^[^a,e,i,o,u]) from mf
     *
     * vv subselects(^[a,e,i,o,u,y](.*)[a,e,i,o,u,y]$) from mf
     * vw subselects(^[a,e,i,o,u,y](.*)[a,e,i,o,u,y]$) from mf
     * cc subselects(^[^a,e,i,o,u](.*)[^a,e,i,o,u]$) from mf
     * cd subselects(^[^a,e,i,o,u](.*)[^a,e,i,o,u]$) from mf
     */
}
