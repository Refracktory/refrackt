package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Pending;
import org.refracktory.refrackt.services.techno.composer.Composition;
import org.refracktory.refrackt.services.techno.tags.Tags;
import org.refracktory.refrackt.templates.Template;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

public class Generation {

    private AtomicInteger version = new AtomicInteger();
    private Log log = Console.console(this);
    private LinkedList<GenerationSet> generations = new LinkedList<>();
    private Tags startingTags;
    private Tags tags;
    private Composition composition;

    public Generation(Template template) {
        composition = new Composition();
        composition.setComposition("needs work!");
        composition.setTemplate(template);
    }

    public GenerationSet current() {
        return generations.peek();
    }

    public GenerationSet startGeneration(Tags existingTags) {
        startingTags = new Tags(existingTags);
        this.tags = new Tags();
        this.tags.add(existingTags.toList());
        GenerationSet gs = new GenerationSet(this, tags, version.getAndIncrement());
        generations.push(gs);
        return gs;
    }

    public GenerationSet next(Tags _tags) {
        tags.add(_tags.toList());
        GenerationSet generationSet = new GenerationSet(this, tags, version.getAndIncrement());
        // this way, each set has the only the properties created up until now:
        generationSet.addProperties(generations.peek().getProperties());
        generations.push(generationSet);
        return generationSet;
    }

    public GenerationSet previous() {
        if (generations.size() == 1) {
            log.warn("already at the end of the generation");
            return generations.peek();
        }
        version.decrementAndGet();
        return generations.pop();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Generation\n");
        sb.append(String.format("--- starting tags--- %s\n", startingTags.asList()));

        for (int i = generations.size()-1 ; i >= 0; i--) {
            GenerationSet gs = generations.get(i);
            sb.append("\n• " + gs);
        }

        sb.append("\n--- final tags --- ");
        sb.append(tags.asList());
        return sb.toString();
    }

    @Pending(description = "this is where you replace stuff in the composition")
    public void compile() {
        tags.add(generations.getFirst().getTags());
        log.error("awaiting compile implementation that does the real work...");
    }

    @Pending(description = "this is getting the whole thing as it is done")
    public Composition getComposition() {
        log.error("awaiting getComposition implementation that actually substitutes values");
        return composition;
    }

    /**
     * @return copy of starting tags.
     */
    public Tags getStartingTags() {
        return new Tags(startingTags.asList());
    }
}
