package org.refracktory.refrackt.services.techno.files;

public class GenerationFile {
    private String selectorName;
    private String text;
    public GenerationFile(String selectorName, String text) {
        this.selectorName = selectorName;
        this.text = text;
    }
    public String getSelectorName() {
        return selectorName;
    }

    public String getText() {
        return text;
    }
}
