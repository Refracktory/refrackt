package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.Console;
import com.knunk.util.Log;

public abstract class Command {
    protected Log log = Console.console(this);
    private boolean active = true;
    private String actualText;

    private String compiledText;

    public abstract void execute(GenerationContext gc);

    public final void executeCommand(GenerationContext gc) {
        if (isActive()) {
            execute(gc);
        } else {
            log.info("not active: '%s'", actualText);
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getActualText() {
        return actualText;
    }

    public void setActualText(String actualText) {
        this.actualText = actualText;
    }

    public void setCompiledText(String compiledText) {
        this.compiledText = compiledText;
    }

    public String getCompiledText() {
        return compiledText;
    }

}
