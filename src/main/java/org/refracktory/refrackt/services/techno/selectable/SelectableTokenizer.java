package org.refracktory.refrackt.services.techno.selectable;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Token;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Tokenizer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Takes select text and turns it into digestible chunks
 */
public class SelectableTokenizer {

  private Log log = Console.console(this);
  private String name;
  private List<SelectableItem> elements = new ArrayList<>();

  /**
   * Expected format: "Text text txt ~generatedValue~ text txt ~otherGenerateValue~ text"
   */
  public SelectableTokenizer(String filename, String text) {
    //
    Tokenizer tokenizer = getSelectLineTokenizer();
    tokenizer.tokenize(text);
    LinkedList<Token> list =  tokenizer.getTokens();

    for (Token token: list) {
      log.info("  ¬[%2d]:%s", token.token, token.sequence.replace("\n", ""));
      if (token.token == 1) {
        elements.add(SelectableItem.convert(token.sequence));
      } else if (token.token == 0) {
        this.name = Parser.tweenParse(token.sequence, "select ", " from");
        log.info("select is named '%s'", name);
      }
    }
    if (this.name == null) {
      name = filename.substring(0, filename.indexOf(".sel"));
    }

    String tokenized = tokenizer.detokenize();
    log.info("\nsucceeded = " + tokenized.equals(text));
  }

  public SelectableTokenizer(String filename, String text, Tokenizer tokenizer) {
    tokenizer.tokenize(text);
    String tokenized = tokenizer.detokenize();
    log.debug("\nsucceeded = " + tokenized.equals(text));
  }

  public static SelectableItemList selectFrom(String text) {
    Tokenizer tokenizer = getSelectLineTokenizer();
    tokenizer.tokenize(text);
    LinkedList<Token> list =  tokenizer.getTokens();
    String name = null;

    List<SelectableItem> elements = new ArrayList<>();
    int startingType = 0;
    for (Token token: list) {
      if (token.token == 66) {
        // skipping comments
        System.out.println("skipped");
      } else if (token.token == 1) {
        elements.add(SelectableItem.convert(token.sequence));
      } else if (token.token == 0) {
        name = Parser.tweenParse(token.sequence, "select ", " from");
      } else if (token.token == -1) {
        name = token.sequence.substring(0, token.sequence.indexOf(" selects from"));
      } else if (token.token == -2) {
        name = token.sequence.substring(0, token.sequence.indexOf(" "));
      } else if (token.token == -3) {
        startingType = -3;
        name = token.sequence.substring(0, token.sequence.indexOf(" "));
        String items = Parser.afterParse(token.sequence, "longselects from ");
        String[] itemsOfSelect = items.split("\\|");
        for (String item: itemsOfSelect) {
          if (item.trim().length() > 0) {
            elements.add(SelectableItem.convert(item));
          }
        }
      }
    }
    if (name == null) {
      throw new IllegalArgumentException("could not determine name of selector from:" + text);
    }
    return new SelectableItemList(name, elements);
  }

  public static void main(String[] args) {
    testLongSelect();
  }

  private static void testSelect() {
    new SelectableTokenizer("code-phrase.sel", "select code-phrase from\n code=1\n xcode=2\n  ycode=1 \n zcode=1\n 'qcode'=2\n yOy=4[evil]{bad}");
  }

  public static void testBasic() {
    new SelectableTokenizer("arbitrary", "#code &code %code $code Text[] texty-txt[0]? ~generatedValue~. \"Yes, * We+ can-()!\"  +- 'certainly(1.0)!' More \\ /text ${groovy} txt ~otherGenerateValue~ text. ^beep4 ^beep4a ^exception? & % #@|", getBasicTokenizer());
  }

  public static void testSubSelect() {
    new SelectableTokenizer("bv", "bv subselects(^[A,E,I,O,U,Y](.*)[a,e,i,o,u,y]$) from st", getSelectLineTokenizer());
  }

  public static void testLongSelect() {
    new SelectableTokenizer("st", "st longselects from | A=5| Ae=1| Ael=1| Bel=1", getSelectLineTokenizer());
  }

  public static Tokenizer getSelectLineTokenizer() {
    Tokenizer tokenizer = new Tokenizer();

    tokenizer.add("select ([a-zA-Z0-9_\\-]+) from", 0);
    tokenizer.add("([a-zA-Z0-9_\\-]+) selects from", -1); // support alternate grammar

    // bv subselects(^[A,E,I,O,U,Y](.*)[a,e,i,o,u,y]$) from st
    tokenizer.add("([a-zA-Z0-9_\\-]+) subselects([a-zA-Z0-9\\^\\[\\]\\(\\)\\.\\*\\$\\,]+) from [a-zA-Z0-9_\\-]+", -2); // support alternate grammar

    // st longselects from | A=5| Ae=1| Ael=1
    tokenizer.add("([a-zA-Z0-9_\\-]+) longselects from [a-zA-Z0-9 \\u0080-\\u9fff_\\-\\,\\=\\|]+", -3); // support alternate grammar

    tokenizer.add("^[\n][\\u0020]", 10);
    tokenizer.add("^[\\p{L}0-9\\''\\s\"\\-\\+\\-\\*\\^\\/\\\\\\&\\%\\|\\$\\[\\]\\(\\)\\\\/{}<>#@.,;:?!~]+[\\=][0-9]+.*", 1);
    tokenizer.add("[\\=][0-9]+", 1);
    tokenizer.add("[\n|\r]", 13); // whitespace and end of lines.
    tokenizer.add("^[#|\\s+#]", 66);
    tokenizer.add("^[\n][a-zA-Z0-9\\s\"\\+\\-\\*\\/\\\\\\&\\%\\|\\$\\[\\]\\(\\)#{}@.,?!~]+[\\=][0-9]+", 2);
    return tokenizer;
  }

  public static Tokenizer getSelectTokenizer() {
    Tokenizer tokenizer = new Tokenizer();

    tokenizer.add("select ([a-zA-Z0-9_\\-]+) from", 0);
    tokenizer.add("[\n|\r]", 13); // whitespace and end of lines.
    tokenizer.add("^[\\u0020][a-zA-Z0-9'\\s\"\\+\\-\\*\\/\\\\\\&\\%\\|\\$\\[\\]\\(\\)#@.,?!]+[\\=][0-9]+", 1);
    tokenizer.add("^[\n][a-zA-Z0-9\\s\"\\+\\-\\*\\/\\\\\\&\\%\\|\\$\\[\\]\\(\\)#@.,?!]+[\\=][0-9]+", 2);
    return tokenizer;
  }

  public static Tokenizer getSelectTokenizernSingleQuoteDoesntWork() {
    Tokenizer tokenizer = new Tokenizer();

    tokenizer.add("select ([a-zA-Z0-9_\\-]+) from", 0);
    tokenizer.add("[\n|\r]", 15); // whitespace and end of lines.
    tokenizer.add("^[\\u0020][a-zA-Z0-9\\s\"'\\+\\-\\*\\/\\\\\\&\\%\\|\\$\\[\\]\\(\\)#@.,?!]+[\\=][0-9]+", 1);
    tokenizer.add("^[\n][a-zA-Z0-9\\s\"'\\+\\-\\*\\/\\\\\\&\\%\\|\\$\\[\\]\\(\\)#@.,?!]+[\\=][0-9]+", 2);
    return tokenizer;
  }

  /**
   * need to expound on this to get non-English letters included:
   * s.matches("(?U)[\\p{L}\\p{M}\\s'-]+")
   */
  public static Tokenizer getBasicTokenizer() {
    Tokenizer tokenizer = null; //new Tokenizer(); // i don't know what happened to this code.  #Sad.

    tokenizer.add("~([a-zA-Z0-9]+)~",       10); // any var
    tokenizer.add("\\$\\{([a-zA-Z0-9]+)\\}",11); // standard replacement template
    tokenizer.add("\\^([a-zA-Z0-9]+)",      12); // special delimit or special sub short-cut
//    tokenizer.add("[a-zA-Z0-9\"'\\+\\-\\*\\/\\\\\\&\\%\\|\\$\\[\\]\\(\\)#@.,?!]+",  13);  // any text & % #@|
    tokenizer.add("[\\p{L}0-9\"'\\+\\-\\*\\/\\\\\\&\\%\\|\\$\\[\\]\\(\\)#@.,?!]+",  13);  // any text & % #@|
    tokenizer.add("[\n|\r]",                14); // next line break
//    tokenizer.add("[\b]",                   15); // texty spaces


//    tokenizer.add("\\.",    10);
//    tokenizer.add(",",      11);
//    tokenizer.add("\\?",    12);
//    tokenizer.add(":",      13);
//    tokenizer.add(";",      14);

//    tokenizer.add("\\(",    20); // open bracket
//    tokenizer.add("\\)",    21); // close bracket
//    tokenizer.add("\\{",    22);
//    tokenizer.add("\\}",    23);

//    tokenizer.add("'",      30);
//    tokenizer.add("\"",     31);

    return tokenizer;
  }

  public List<SelectableItem> getEntries() {
    return elements;
  }

  public SelectableItemList getSelectableItemList() {
    return new SelectableItemList(name, elements);
  }
}
