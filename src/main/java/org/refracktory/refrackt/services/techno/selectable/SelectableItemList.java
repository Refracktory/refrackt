package org.refracktory.refrackt.services.techno.selectable;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import org.refracktory.randomness.RandomUtil;
import org.refracktory.refrackt.services.techno.parser.TaggedStringElement;
import org.refracktory.refrackt.services.techno.tags.Tags;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SelectableItemList implements TaggedStringElement {

  private String name;
  private List<SelectableItem> list = new ArrayList<>();
  private Log log = Console.console(this);

  public String getName() {
    return name;
  }

  public SelectableItemList(final String name, List<SelectableItem> plist) {
    if (plist == null || plist.size() == 0) {
      log.error("'%s' has no elements in the current selection. check the tagging", name);
    }
    this.name = name;
    this.list = plist;
    setupRanges(this.list);
  }

  private SelectableItemList(List<SelectableItem> plist) {
    this.list = plist;
    setupRanges(this.list);
  }

  public List<SelectableItem> apply(Tags tags) {
    return list.stream().filter(selectableItem -> selectableItem.isSelectedBy(tags)).collect(Collectors.toList());
  }

  public Selected select(Tags tags) {
    List<SelectableItem> selectedList = new ArrayList<>();
    for (SelectableItem selectableItem : list) {
      if (selectableItem.isSelectedBy(tags))
      {
        log.debug("adding %s", selectableItem);
        selectedList.add(selectableItem);
      }
    }
    SelectableItemList slist = new SelectableItemList(name, selectedList);
    return slist.select();
  }

  public Selected selectAndRemove(Tags tags) {
    List<SelectableItem> selectedList = new ArrayList<>();
    for (SelectableItem selectableItem : list) {
      if (selectableItem.isSelectedBy(tags))
      {
        log.debug("adding %s", selectableItem);
        selectedList.add(selectableItem);
      }
    }
    SelectableItemList slist = new SelectableItemList(name, selectedList);
    Selected selected = slist.select();
    list.remove(selected.getSelectedItem());
    return selected;
  }

  public Selected select() {
    double count = 0;
    count = setupRanges(list);
    double result = RandomUtil.random() * count;
    return find(result);
  }

  private Selected find(double value) {
    for (SelectableItem selectableItem : list) {
      if (selectableItem.isSelectedby(value)) {
        return new Selected(name, selectableItem.getValue(), selectableItem.getTags(), selectableItem);
      }
    }

    return new Selected(name,"*", new Tags("error_condition: " + value), null);
  }

  private double setupRanges(List<SelectableItem> items) {

    try {
      double count = 0.0;
      for (SelectableItem item : items) {
        double lower = count;
        count += item.getWeight();
        double upper = count;

        item.setLower(lower);
        item.setUpper(upper);
        //      Range range = new Range(lower, upper);
      }
      return count;
    } catch (Exception e) {
      log.error("items:" + items);
      e.printStackTrace();
    }
    return 0.0;
  }

  @Deprecated
  @Override public String eval() {
    return select().getText();
  }

  @Deprecated
  @Override public String eval(Tags tags) {
    return select(tags).getText();
  }

  public int size() {
    return list.size();
  }

  public List<SelectableItem> all() {
     return list;
  }

    public SelectableItemList subselect(String name, String regex) {
      List<SelectableItem> regexList = new ArrayList<>();
      for (SelectableItem si : list) {
        if (Parser.matchesWithRegex(si.getValue(), regex)) {
          regexList.add(si);
        }
      }
      return new SelectableItemList(name, regexList);
    }

  public SelectableItemList makeCopy() {
    List<SelectableItem> copy = new ArrayList<>();
    for (SelectableItem si: list) {
      if (si != null) {
        copy.add(si.copy());
      } else {
        log.debug("removed null selectable");
      }
    }
    return new SelectableItemList(name, copy);
  }
}
