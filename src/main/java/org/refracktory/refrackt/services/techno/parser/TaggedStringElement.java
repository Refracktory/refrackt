package org.refracktory.refrackt.services.techno.parser;

import org.refracktory.refrackt.services.techno.tags.Tags;

public interface TaggedStringElement {
  String eval();
  String eval(Tags tags);
}
