package org.refracktory.refrackt.services.techno;

import com.knunk.util.AbstractPersistentProperties;
import com.knunk.util.Console;
import com.knunk.util.Log;
import org.refracktory.refrackt.exceptions.SelectableMissingException;
import org.refracktory.refrackt.exceptions.TemplateMissingException;
import org.refracktory.refrackt.repository.SelectablesRepository;
import org.refracktory.refrackt.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class SelectLoadService {

  private Log log = Console.console(this);

  @Autowired
  private SelectablesRepository selectablesRepository;

  @Autowired
  private TemplateRepository templateRepository;

  @Autowired
  private AbstractPersistentProperties applicationProperties;

  @PostConstruct
  public void postConstruct() {
    log.info("loaded %d selectables from '%s'", selectablesRepository.count(), applicationProperties.get("selectables_directory"));
    List<String> keys = selectablesRepository.keySet();
    for (String key: keys) {
      try {
        log.info("- %3d %s", selectablesRepository.get(key).size(), key);
      } catch (SelectableMissingException e) {
        log.error("couldn't load %s", key);
      }
    }

    log.info("loaded %d templates from '%s'", templateRepository.count(), applicationProperties.get("templates_directory"));
    List<String> tkeys = templateRepository.keySet();
    for (String tkey: tkeys) {
      try {
        log.info("- %6d %s", templateRepository.get(tkey).getText().length(), tkey);
      } catch (TemplateMissingException e) {
        log.error("couldn't load %s", tkey);
      }
    }

  }

}
