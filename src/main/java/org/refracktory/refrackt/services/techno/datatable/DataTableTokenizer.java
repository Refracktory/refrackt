package org.refracktory.refrackt.services.techno.datatable;

import com.knunk.util.Console;
import com.knunk.util.Log;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Token;
import org.refracktory.refrackt.services.techno.parser.tokenizer.Tokenizer;
import org.refracktory.refrackt.util.Constants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Takes select text and turns it into digestible chunks
 */
public class DataTableTokenizer {

  private static Log log = Console.console(DataTableTokenizer.class);
  private String name;
  private List<DataTableItem> elements = new ArrayList<>();

  public DataTableTokenizer(String filename, String text) {
    //
    Tokenizer tokenizer = getDataTableTokenizer();
    tokenizer.tokenize(text);
    LinkedList<Token> list =  tokenizer.getTokens();

    for (Token token: list) {
      log.info("  ¬[%2d]:%s", token.token, token.sequence.replace("\n", ""));
      if (token.token == 1) {
        elements.add(DataTableItem.convert(token.sequence));
      } else if (token.token == 0) {
      }
    }
    if (this.name == null) {
      name = filename.substring(0, filename.indexOf(".data"));
    }

    String tokenized = tokenizer.detokenize();
    log.info("\nsucceeded = " + tokenized.equals(text));
  }

  public DataTableTokenizer(String filename, String text, Tokenizer tokenizer) {
    tokenizer.tokenize(text);
    String tokenized = tokenizer.detokenize();
    log.debug("\nsucceeded = " + tokenized.equals(text));
  }

  public static DataTable dataizeFrom(String text) {
    Tokenizer tokenizer = getDataTableTokenizer();
    tokenizer.tokenize(text);
    LinkedList<Token> list =  tokenizer.getTokens();
    String name = null;
    String dataType = null;
    String referencedBy = null;
    List<DataTableItem> elements = new ArrayList<>();
    for (Token token: list) {
      if (token.token == 0) {
        DataTableItem item = DataTableItem.convert(token.sequence);
        if (name == null) {
          name = item.getName();
          // FIXME attempting to move individual data items up into individual data table, so this is fluid:
          //name = item.getDataType();
        }
        if (dataType == null) {
          // attempting to move individual data items up into individual data table, so this is fluid:
//          if (item.getReferencedBy() != null) {
//            name = item.getDataType();
//          }
          dataType = item.getDataType();
        }
        if (item.getReferencedBy() != null) {
          referencedBy = item.getReferencedBy();
        }
        elements.add(item);
      } else if (token.token == 1) {
      } else if (token.token == -1) {
      }
    }
    if (dataType == null) {
      throw new IllegalArgumentException("could not determine name of datatable from:" + text);
    }
    DataTable dt = new DataTable(name, elements);
    if (dataType != null) {
      dt.setDataType(dataType);
    }
    if (referencedBy != null) {
      dt.setReferencedBy(referencedBy);
    }
    return dt;
  }

  public static void main(String[] args) {
    String text = "studded leather is light armor with AC=3|absorb=3|deflect=0|deslash=2|weight=0.1*weight|cost=20|class=light\n" +
            "plate is heavy armor with AC=9|absorb=5|deflect=5|deslash=5|weight=0.22*~pc-weight~|cost=1000|class=heavy\n" +
            "leather is light and flexible armor with AC=3|absorb=1|deflect=0|deslash=1|weight=10|cost=10|class=light\n" +
            "padding is armor with AC=1|absorb=1|deflect=1|deslash=1|weight=5|cost=1|class=light\n" +
            "mail is medium armor with AC=5|absorb=1|deflect=1|deslash=5|weight=13|cost=100|class=medium\n";
    log.info(DataTableTokenizer.dataizeFrom(text).toString());
  }

  /**
   * if you change this, you need to update the <code>DataTableItem#getDataTableItemTokenizer</code>
   * @return
   */
  public static Tokenizer getDataTableTokenizer() {
    Tokenizer tokenizer = new Tokenizer();
    tokenizer.add("([a-zA-Z0-9_\\-\u0020\\[\\]]+) is ([,a-zA-Z0-9_\\-\u0020]+) with ([a-zA-Z0-9_|=\\*\\~\\.\\-\\+]+)", 0);
    tokenizer.add("^[\n][\\u0020]", 10);
    tokenizer.add("^#.*", Constants.COMMENT_LINE); // skip line
    tokenizer.add("[\n|\r]", 13); // whitespace and end of lines.
    return tokenizer;
  }

  /**
   *         tokenizer.add("([a-zA-Z0-9_\\-]+) is ", 0);
   *         tokenizer.add("([a-zA-Z0-9_\\-, ]+) with ", 1);
   *         tokenizer.add("([a-zA-Z0-9_\\-|=]+)", 2);
   *         tokenizer.add("^[\n][\\u0020]", 10);
   *         tokenizer.add("[\n|\r]", 13); // whitespace and end of lines.
   */

  public List<DataTableItem> getEntries() {
    return elements;
  }

  @Override
  public String toString() {
    return String.format("%s{%s}", name, elements);
  }
}
