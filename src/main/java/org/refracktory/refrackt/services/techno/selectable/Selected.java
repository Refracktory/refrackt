package org.refracktory.refrackt.services.techno.selectable;

import org.refracktory.refrackt.services.techno.tags.Tags;

public class Selected {
    private String name;
    private String text;
    private Tags generatedTags;

    public SelectableItem getSelectedItem() {
        return selectableItem;
    }

    private SelectableItem selectableItem;

    public Selected(String name, String text, Tags generatedTags, SelectableItem selectableItem) {
        this.name = name;
        this.text = text;
        this.generatedTags = generatedTags;
        this.selectableItem = selectableItem;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public Tags getGeneratedTags() {
        return generatedTags;
    }

    @Override
    public String toString() {
        return String.format("%s: '%s', GENERATES[%s]", name, text, generatedTags);
    }
}
