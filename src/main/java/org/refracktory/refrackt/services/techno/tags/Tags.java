package org.refracktory.refrackt.services.techno.tags;

import com.knunk.util.Console;
import com.knunk.util.Log;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Tags {

  public final static String DELIMITER = ",";

  public final static String LEFT_DELIMITER = "[";
  public final static int LEFT_DELIMITER_SIZE = LEFT_DELIMITER.length();

  public final static String RIGHT_DELIMITER = "]";
  public final static int RIGHT_DELIMITER_SIZE = RIGHT_DELIMITER.length();

  public final static String ASSIGNMENT_OPERATOR = "=";
  public final static int ASSIGNMENT_OPERATOR_SIZE = ASSIGNMENT_OPERATOR.length();

  private Log log = Console.console(this);

  private Set<String> _tags = new HashSet<>();
  //TODO THIS IS FOR TAGS THAT ASSIGN VALUES TO STUFF!!
  private Assignments assignments = new Assignments();

  public Tags() {
    // tsilb
  }

  public Tags(List<String> tags) {
    add(tags.toArray(new String[]{}));
  }

  public Tags(Tags tags) {
    String[] entries = tags.toList();
    add(entries);
  }

  public Tags(String tags) {
    String[] entries = tags.split(DELIMITER);
    add(entries);
  }

  public void add(final String[] tags) {
    for (String tag :tags) {
      add(tag.toUpperCase());
    }
  }

  public boolean add(final String tag) {
    if (tag.contains(ASSIGNMENT_OPERATOR))
    {
      String[] pair = tag.split(ASSIGNMENT_OPERATOR);
      assignments.put(tag);
      return false;
    }
    if (!_tags.contains(tag)) {
//      log.info("added tag '%s'", tag);
      return _tags.add(tag.toUpperCase());
    }
    return false;
  }

  public boolean remove(final String tag) {
    return _tags.remove(tag.toUpperCase());
  }

  public boolean contains(final String tag) {
    return _tags.contains(tag);
  }

  public boolean containsAll(final List<String> tags) {
    List<String> utags = tags.stream().map(String::toUpperCase).collect(Collectors.toList());
    for (final String tag: utags) {
      if (!_tags.contains(tag)) {
        return false;
      }
    }
    return true;
  }

  public boolean containsAnyOf(final List<String> tags) {
    List<String> utags = tags.stream().map(String::toUpperCase).collect(Collectors.toList());
    for (final String tag: utags) {
      if (_tags.contains(tag)) {
        return true;
      }
    }
    return false;
  }

  public boolean containsNoneOf(final List<String> tags) {
    List<String> utags = tags.stream().map(String::toUpperCase).collect(Collectors.toList());
    for (final String tag: utags) {
      if (_tags.contains(tag)) {
        return false;
      }
    }
    return true;
  }

  @Override public String toString() {
    return "Tags{" +
      "_tags=" + _tags +
      ", assignments=" + assignments +
      '}';
  }

  public String toMiniString() {
    return (_tags != null && _tags.size() > 0 ? _tags.toString() : "")  + assignments.toMiniString();
  }

  public String[] toList() {
    return _tags.toArray(new String[]{});
  }

  public String asList() {
    return _tags.stream().map(String::toUpperCase).collect(Collectors.toList()).toString();
  }

  public boolean exist() {
      return _tags != null && _tags.size() > 0;
    }

  public boolean negateOnly() {
    // this is all guessing...
    if (_tags.size() == 0) {
      return false;
    }
    for (String tag: _tags) {
      if (!tag.startsWith("!")) {
        return false;
      }
    }
    return true;
  }

  public Assignments getAssignments() {
    return assignments;
  }

  public void add(Tags tags) {
    for (String tag : tags.toList()) {
      _tags.add(tag.toUpperCase());
    }
    tags.getAssignments().getAssignmentProperties();
  }

    public void clear() {
      _tags.clear();
    }
}
