package org.refracktory.refrackt.services.techno.assigned;

import org.refracktory.refrackt.services.techno.generation.Command;
import org.refracktory.refrackt.services.techno.generation.GenerationContext;

public class AssignTo extends Command {
    private final String name;
    private final String operation;

    public AssignTo(String name, String operation) {
        this.name = name;
        this.operation = operation;
    }

    public String getName() {
        return name;
    }

    public String getOperation() {
        return operation;
    }

    @Override
    public void execute(GenerationContext gc) {
        gc.getProperties().set(name, "??");
    }
}
