package org.refracktory.refrackt.services.techno.assigned;

public class AssignToCalculation extends AssignTo {

    private final String reference;

    public AssignToCalculation(String name, String dataBasedOn, String operation) {
        super(name, operation);
        this.reference = dataBasedOn;
    }

    public String getReference() {
        return reference;
    }

}
