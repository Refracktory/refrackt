package org.refracktory.refrackt.services.techno.generation;

import com.knunk.util.AbstractPersistentProperties;
import com.knunk.util.Console;
import com.knunk.util.Log;
import org.refracktory.refrackt.services.techno.tags.Tags;

import java.util.Arrays;
import java.util.Properties;

public class GenerationContext {
    private Log log = Console.console(this);
    private Tags tags = new Tags();
    private AbstractPersistentProperties properties = new AbstractPersistentProperties();
    private String template;

    public String getContext() {
        return context;
    }

    private String context;

    public GenerationContext(String template) {
        this.template = template;
    }

    public AbstractPersistentProperties getProperties() {
        return properties;
    }

    public Tags getTags() {
        return tags;
    }

    public void addTags(Tags tags) {
        log.info("somebody tagged me with %s", tags.toMiniString());
        this.tags.add(tags);
    }

    public void addProperties(Properties properties) {
        this.properties.putAll(properties);
    }

    public boolean containsKey(String key) {
        return properties.containsKey(key);
    }
    public String get(String key) {
        return properties.get(key);
    }

    public void clearTags() {
        tags.clear();
    }

    public void resetTagsTo(Tags tags) {
        this.tags = new Tags(Arrays.asList(tags.toList()));
    }

    public void setContext(String context) {
        this.context = context;
    }

    public GenerationContext clone() {
        log.warn("not actually cloning this gc instance.");
        return this;
    }
}
