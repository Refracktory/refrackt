package org.refracktory.refrackt.services.techno.todo;

public class ComposedOfAssignTo {
    /**
     # form the full name.  ! forces the variable to be included, ? before a variable means skip it if it is not set.
     name composed of !first?surname
     #name1 assigned via first
     #name2 composed of !first?second?surname
     #name3 composed of !first !Q!s!Q?surname
     */
}
