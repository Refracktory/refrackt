package org.refracktory.refrackt.services.techno.render;

import com.knunk.util.*;
import org.refracktory.refrackt.util.Utility;

public class Render {
    public static final String END_OF_LINE_CHARACTER = System.getProperty("line.separator");
    public static final String FUNC_LEFT_CONSTRAINT_DELIMITER = "{";
    public static final String FUNC_RIGHT_CONSTRAINT_DELIMITER = "}";
    private Log console = Console.console(this);

    public String rend(String text)  {
        final StringBuilder resulting = new StringBuilder();
        String[] lines = text.split("\n");
        for (String line : lines)
        {
            int count = 100;
            String oldline = null;
            while(!line.equals(oldline) && count-- > 0)
            {
                oldline = line.toString();
                final String function = Parser.tweenParse(line, "@", FUNC_LEFT_CONSTRAINT_DELIMITER);
                final String args     = Parser.tweenParse(line, FUNC_LEFT_CONSTRAINT_DELIMITER, FUNC_RIGHT_CONSTRAINT_DELIMITER);
                final String func     = "@" + function + FUNC_LEFT_CONSTRAINT_DELIMITER + args + FUNC_RIGHT_CONSTRAINT_DELIMITER;
                NestedUnwinder nw = new NestedUnwinder(line,"{}");
                String result0 = nw.unwind(new UnwinderFunctionSolver());
                line = TextUtility.replaceStuffAtEnd(result0);
                if (line.contains("@") && line.contains("{"))
                {
                    final String res = solve("@"+function, args);
                    line = Parser.findAndReplaceFirst(line, func, res);
                }
            }
            resulting.append(line + END_OF_LINE_CHARACTER);
        }
        return resulting.toString();
    }

    private static class UnwinderFunctionSolver implements Solver
    {

        @Override
        public String solve(String function, String insides)
        {
            final String line = function + FUNC_LEFT_CONSTRAINT_DELIMITER + insides + FUNC_RIGHT_CONSTRAINT_DELIMITER;
            return Utility.doFunctions(line);
        }

    }

    public String solve(final String passedFunction, final String args)
    {
        //FIXME HACK!
        if (!passedFunction.contains("@"))
        {
            return passedFunction + "{" + args + "}";
        }
        // tack it back if it isn't the if/endif
        StringBuilder sb = new StringBuilder();

        String result = new String(passedFunction);

        // not sure if this needs to be nested.
        if (result.contains("@, "))
        {
            sb.append("@, ");
            // get rid of @, as it is passed thru sb
            result = result.replaceFirst("@, ", "");
        }

        final String beginningStuff = result.substring(0, result.indexOf("@"));
        final String remaining = result.substring(result.indexOf("@"));
        // handle 'id' style
        String function = Parser.takeUptoDelimiters(remaining," {");
        String id = "";
        String clazz = " ";
        String clazzOnly = "";
        if (function.contains("#"))
        {
            id = " id=\"" + Parser.afterParse(function, "#").trim() + "\"";
            function = function.substring(0, function.indexOf("#"));

        }
        else if (function.contains("."))
        {
            clazzOnly = " " + Parser.afterParse(function, ".").trim();
            function = function.substring(0, function.indexOf("."));
            clazz = " class=\"" + clazzOnly.trim() + "\"";
        }
        else
        {
            function = remaining;
        }

        if (function.equals("@href"))
        {
            final String[] cmds = args.split(",");
            final String href = cmds[0];
            final String label = cmds[1];
            final String pointer = "<a target=\"_blank\" href=\"" + href + "\">" + label + "</a>";
            sb.append(pointer);
        }
        else
        {
            console.debug("processing general functions");
            String res = Utility.doFunctions(function + "{" + args + "}");
//            sb.append(res);

            if (res.contains("@")) {
                res = TextUtility.replaceEnglishStuff(res);
            }
            sb.append(res);
        }
        return beginningStuff + sb.toString();
    }

}
