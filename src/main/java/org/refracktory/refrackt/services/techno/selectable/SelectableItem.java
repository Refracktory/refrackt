package org.refracktory.refrackt.services.techno.selectable;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Range;
import org.apache.commons.lang3.StringUtils;
import org.refracktory.refrackt.services.techno.criteria.Criteria;
import org.refracktory.refrackt.services.techno.tags.Tags;

public class SelectableItem implements Comparable<SelectableItem> {
  public final static String WEIGHTING_DIVIDER = "=";
  public final static int WEIGHTING_DIVIDER_SIZE = WEIGHTING_DIVIDER.length();
  public final static String LEFT_ASSIGNMENT_DELIMITER = "[";

  private static Log log = Console.console(SelectableItem.class);

  private final String value;

  private final Double weight;
  private final Criteria criteria;

  public Tags getTags() {
    return tags;
  }

  private final Tags tags;
  private final Range range = new Range();

  public SelectableItem copy() {
    return new SelectableItem(value, weight, criteria, tags);
  }

  public SelectableItem(String value, Double weight, Criteria criteria, Tags tags) {
    this.value = value;
    this.weight = weight;
    this.criteria = criteria;
    this.tags = tags;
  }

  public String getValue() {
    return value;
  }

  public Double getWeight() {
    return weight;
  }

  public boolean isSelectedBy(Tags tags) {
    if (criteria == null) {
      if (tags.negateOnly()) {
        return false;
      } else {
        return true;
      }
    }
    return this.criteria.match(tags);
  }

  public boolean isSelectedby(double value) {
    return range.contains(value);
  }
  /**
   * Convert a full line of select text into a selectable item.
   * @param simpleLineOfText 'value'='number'['constraints']{'tags added to parent object'}
   * @return a selectable item.
   */
  public static SelectableItem convert(final String simpleLineOfText) {

    int endOfPostWeighting = simpleLineOfText.length();
    Criteria criteria = null;
    Tags tags = null;

    try {
      if (simpleLineOfText.contains(Tags.LEFT_DELIMITER) && simpleLineOfText.contains(Tags.RIGHT_DELIMITER)) {
        final String tagText = simpleLineOfText.substring(
          simpleLineOfText.lastIndexOf(Tags.LEFT_DELIMITER) + Tags.LEFT_DELIMITER_SIZE,
          simpleLineOfText.indexOf(Tags.RIGHT_DELIMITER));
        tags = new Tags(tagText);

        // update end of line
        endOfPostWeighting = simpleLineOfText.lastIndexOf(Tags.LEFT_DELIMITER);
      } else {
        tags = new Tags();
      }

      if (simpleLineOfText.contains(Criteria.LEFT_DELIMITER) && simpleLineOfText.contains(Criteria.RIGHT_DELIMITER)) {
        final String criteriaText = simpleLineOfText.substring(
          simpleLineOfText.lastIndexOf(Criteria.LEFT_DELIMITER) + Criteria.LEFT_DELIMITER_SIZE,
          simpleLineOfText.lastIndexOf(Criteria.RIGHT_DELIMITER));
        criteria = new Criteria(criteriaText);

        // update end of line
        endOfPostWeighting = simpleLineOfText.lastIndexOf(Criteria.LEFT_DELIMITER);
      }

      // the remaining text is just the value and the weight
      final String remainingText = simpleLineOfText.substring(0, endOfPostWeighting);
      String weightString = remainingText.substring(remainingText.lastIndexOf(WEIGHTING_DIVIDER) + WEIGHTING_DIVIDER_SIZE);
      if (weightString.contains(LEFT_ASSIGNMENT_DELIMITER)) {
        weightString = weightString.substring(0, weightString.indexOf(LEFT_ASSIGNMENT_DELIMITER));
      }
      final Double weight = Double.parseDouble(weightString);
      // finally, the value remains! trim off leading space per format.
      final String value = remainingText.substring(0, remainingText.lastIndexOf(WEIGHTING_DIVIDER));

      SelectableItem selectableItem = new SelectableItem(value, weight, criteria, tags);
      return selectableItem;
    } catch (StringIndexOutOfBoundsException siobe) {
      log.error("failed to parse tags in %s : %s", simpleLineOfText, siobe.getMessage());
    }
    return null;
  }

  @Override public String toString() {
    return "SelectableItem{" +
      "value='" + value + '\'' +
      ", weight=" + weight +
      ", criteria=" + criteria +
      ", tagsToAdd=" + tags +
      '}';
  }

  public String toListItem() {
    return value + '=' + Math.round(weight) +
             (criteria != null ? "{" +criteria + "}" : "")
            + (tags != null ? tags.toMiniString() : "");
  }

  public static SelectableItem testBasic() {
    SelectableItem si = SelectableItem.convert("red=1");
    return si;
  }

  public static void test() {
    System.out.println(SelectableItem.convert(" red=1"));
    System.out.println(SelectableItem.convert(" blue=2{!red}"));
    System.out.println(SelectableItem.convert(" green=3{!red,!blue}"));
    System.out.println(SelectableItem.convert(" yellow=4[coward]"));
    System.out.println(SelectableItem.convert(" mauve=5{red}[coward]"));
    System.out.println(SelectableItem.convert(" purple=6{red|blue}[royal]"));
    System.out.println(SelectableItem.convert(" puce=7{red|blue}[royal,size=large]"));
    // not sure what the point of these two are:
//    System.out.println(SelectableItem.convert("pomegranite{puce}=8{red|blue}[royal,size=large]"));
//    System.out.println(SelectableItem.convert("pomegranite{puce}[1]=9{red|blue}[royal,size=large]"));
  }

  public static void main(String...args) {
    test();
  }

  public void setLower(double count) {
    range.setLowerBound(count);
  }

  public void setUpper(double count) {
    range.setUpperBound(count);
  }

  public boolean isTagless() {
    return tags == null || !tags.exist();
  }

  @Override
  public int compareTo(SelectableItem that) {
    return StringUtils.compare(this.getValue(), that.getValue());
  }
}
