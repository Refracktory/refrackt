package org.refracktory.refrackt.services.techno.parser;

import com.knunk.util.Console;
import com.knunk.util.FileUtil;
import com.knunk.util.Log;
import com.knunk.util.Parser;

import javax.annotation.PostConstruct;
import java.io.File;

public class FullFileParser {

    public static String SELECT_1 = "^select ([a-zA-Z0-9_\\-]+) from";
    public static String SELECT_2 = "^([a-zA-Z0-9_\\-]+) selects from"; // support alternate grammar

    private Log log = Console.console(FullFileParser.class);

    @PostConstruct
    public void getSelectorFromFile(String fullFileName) {
        String selectorName = fullFileName;
        selectorName = selectorName.substring(selectorName.lastIndexOf(File.separator)+1);
        selectorName = selectorName.substring(0, selectorName.indexOf("."));

        String text = FileUtil.readFileWithCR(fullFileName);
        // windows processing
        text = text.replace("\r", "\n");

        parseTheLines(text.split("\n"));

    }

    private void parseTheLines(String[] lines) {
        boolean inCommand = false;
        for (String text : lines) {
            if (Parser.matchesWithRegex(text, "^#")) {
            }
            if (Parser.matchesWithRegex(text, SELECT_1)) {
                log.info("style 1 selector");
                // continue processing till next command string
            } else if (Parser.matchesWithRegex(text, SELECT_2)) {
                log.info("style 2 selector");
            }
        }
    }

}
