package org.refracktory.refrackt.services.techno.parser;

import org.refracktory.refrackt.services.techno.tags.Tags;

public class Text implements TaggedStringElement {
  private String text;

  public Text(String text) {
    this.text = text;
  }

  @Override public String eval() {
    return text;
  }

  @Override public String eval(Tags tags) {
    return text;
  }
}
