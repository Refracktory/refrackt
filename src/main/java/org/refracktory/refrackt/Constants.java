package org.refracktory.refrackt;

public class Constants {
    public static String SELECT_1 = "^select ([a-zA-Z0-9_\\-]+) from";
    public static String SELECT_2 = "^([a-zA-Z0-9_\\-]+) selects from"; // support alternate grammar
    public static String SUBSELECT = "([a-zA-Z0-9_\\-]+) subselects([a-zA-Z0-9\\^\\[\\]\\(\\)\\.\\*\\$\\,]+) from [a-zA-Z0-9_\\-]+";
    public static String LONGSELECT = "([a-zA-Z0-9_\\-]+) longselects from [a-zA-Z0-9_\\-\\,\\=\\|]+";
}
