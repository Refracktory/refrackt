package org.refracktory.refrackt;

//import com.knunk.util.AbstractPersistentProperties;

import com.knunk.util.AbstractPersistentProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Refracktor {

  @Value("${refrackt_filename:refrackt.properties}")
  private String filename;

  public static void main(String[] args) {
    SpringApplication.run(Refracktor.class, args);
  }

  @Bean
  public AbstractPersistentProperties applicationProperties() {
    return new AbstractPersistentProperties("application.properties");
  }

}
