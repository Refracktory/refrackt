package org.refracktory.refrackt.util;

public class BuildLog {
    private StringBuilder log = new StringBuilder();
    private StringBuilder errors = new StringBuilder();
    private static BuildLog currentBuildLog = new BuildLog();

    public static BuildLog getBuildLog() {
        return currentBuildLog;
    }

    public void addLine(String line) {
        log.append(line);
        log.append(System.lineSeparator());
    }

    public void addError(String line) {
        log.append(line);
        log.append(System.lineSeparator());
        errors.append(line);
        errors.append(System.lineSeparator());
    }

    public String getLogText() {
        return log.toString();
    }

    public String getErrorText() {
        return errors.toString();
    }

    public void console() {
        System.out.println(getLogText());
    }

    public void messages() {
        System.out.println("errorlog:\n" + getErrorText());
    }
}
