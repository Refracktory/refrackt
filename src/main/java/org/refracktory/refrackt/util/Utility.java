package org.refracktory.refrackt.util;

import com.knunk.util.Console;
import com.knunk.util.Log;
import com.knunk.util.Parser;
import com.knunk.util.TextUtility;
import org.refracktory.randomness.Roll;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

public class Utility
{
    private static final String DICE_FUNCTION = "dice(";
    private static final String DICE_DROP_LOWEST_FUNCTION = "diceDropLowest(";
    private static final String DICE_DROP_HIGHEST_FUNCTION = "diceDropHighest(";
    public static final String CALC_DELIMITER 				= "@calc";
    private static Log console = Console.console(Utility.class);

    public static File getCurrentDir()
    {
        final String currentDir = getCurrentDirString();
        return new File(currentDir);
    }

    public static String getCurrentDirString()
    {
        final String currentDir = System.getProperty("user.dir");
        return currentDir;
    }

    public static String processDice(String rhs, String result)
    {
        if (result.contains(DICE_FUNCTION))
        {
            while (result.contains(DICE_FUNCTION))
            {
                final String dice = Parser.tweenParse(result, DICE_FUNCTION, ")");
                result = Parser.findAndReplace(result, DICE_FUNCTION + dice + ")", "" + new Roll(dice).toInt());
                console.debug("evaluate %s = %s", rhs, result);
            }
        }
        if (result.contains(DICE_DROP_LOWEST_FUNCTION))
        {
            while (result.contains(DICE_DROP_LOWEST_FUNCTION))
            {
                final String dice = Parser.tweenParse(result, DICE_DROP_LOWEST_FUNCTION, ")");
                result = Parser.findAndReplace(result, DICE_DROP_LOWEST_FUNCTION + dice + ")", "" + new Roll(dice).dropLowest().toInt());
                console.debug("evaluate %s = %s", rhs, result);
            }
        }
        if (result.contains(DICE_DROP_HIGHEST_FUNCTION))
        {
            while (result.contains(DICE_DROP_HIGHEST_FUNCTION))
            {
                final String dice = Parser.tweenParse(result, DICE_DROP_HIGHEST_FUNCTION, ")");
                result = Parser.findAndReplace(result, DICE_DROP_HIGHEST_FUNCTION + dice + ")", "" + new Roll(dice).dropHighest().toInt());
                console.debug("evaluate %s = %s", rhs, result);
            }
        }
        return result;
    }
    /**
     * Handles diced randoms. maybe not the best idea.
     *
     * @param value
     * @return
     */
    public static Double valueParse(final String value)
    {
        if (value.contains("dice(") || value.contains("diceDropLowest(") || value.contains("diceDropHighest("))
        {
            String diceString = "" + Integer.parseInt(Utility.processDice(value, value));
            final double result = Double.parseDouble(diceString);
            return result;
        }
        try
        {
            Double result = Double.parseDouble(value);
            return result;
        }
        catch (NumberFormatException nfe)
        {
            return null;
        }

    }

    public static List<String> parseOutVariables(final String lineOfText)
    {
        return parseOutVariables(lineOfText, '~');
    }
    /**
     * Parse out all of the variables in the text and reverse their order so that shorter variables can be replaced first.
     * @param lineOfText
     * @return all the variables
     */
    public static List<String> parseOutVariables(final String lineOfText, final char delimiter)
    {
        final String[] subs = Parser.splitOutItemsWithin(lineOfText, delimiter, delimiter);
        List<String> list = new ArrayList<String>();
        for (String string : subs)
        {
            list.add(string);
        }
        List<String> result = Parser.defuseSubstitution(list);
        return result;
    }

    /**
     * Execute a text function (@function) with an argument delimited by [ ]
     * @param text the text of the function
     * @return the result of the function
     */
    public static String doFunctions(final String text)
    {
        if (!text.contains("@")) {
            return text;
        }
    	final String intermediate = doFunctions(text, "{", "}");
    	return intermediate;
    }

	public static String doFunctions(final String text, final String leftDelim, final String rightDelim)
    {
        if (!text.contains("@"))
        {
            return text;
        }

        String values = Parser.tweenParse(text, leftDelim, rightDelim);
        console.debug("functionalizing '%s', values='%s'", text, values);
        values = Parser.reduceTextyMaths(values);

        if (text.startsWith(CALC_DELIMITER))
        {
            //FIXME? if something is not defined, make it 0!
            final String[] subs = Parser.splitOutItemsWithin(values, Constants.NOT_SUBSTITUTED_C, Constants.NOT_SUBSTITUTED_C);
            List<String> list = new ArrayList<String>(Arrays.asList(subs));
            Collections.sort(list);
            for (String sub : list)
            {
                values = values.replace(Constants.NOT_SUBSTITUTED_C + sub + Constants.NOT_SUBSTITUTED_C, "0");
            }
            String calc = Parser.evalAsText(values);
            console.debug("  = '%s'", "" + calc);
            return Parser.integerizeWholeNumber(calc);
        }
        else if (findCaselessMatch(text, "@capitalizeEach"))
        {
            return Parser.capitalizeEachWord(values);
        }
        else if (findCaselessMatch(text, "@decapitalize"))
        {
            if (values.length() > 1) {
                return values.substring(0, 1).toLowerCase() + values.substring(1);
            }
            return values;
        }
        else if (findCaselessMatch(text, "@capitalize"))
        {
            return Parser.capitalize(values);
        }
        else if (findCaselessMatch(text, "@popup"))
        {
            return "<a href='link/info?spell=fireball'>" +values+ "</a>";
        }
        else if (findCaselessMatch(text, "@dice"))
        {
            final String result = "" + new Roll(values).toInt();
            return result;
        }
//        else if (findCaselessMatch(text, "@calc"))
//        {
//            final String result = Parser.evalAsText(values);
//            return result;
//        }
        else if (findCaselessMatch(text, "@upper"))
        {
            return values.toUpperCase();
        }
        else if (findCaselessMatch(text, "@lower"))
        {
            return values.toLowerCase();
        }
        else if (findCaselessMatch(text, "@wordify"))
        {
            return TextUtility.convertToWords(values);
        }
        else if (findCaselessMatch(text, "@minimum"))
        {
            console.info("taking minimum of %s", values);
            final String[] pair = values.split(",");

            double left = Parser.eval(pair[0]);
            double right = Parser.eval(pair[1]);
            return Parser.integerizeWholeNumber(Math.min(left, right));
        }
        else if (findCaselessMatch(text, "@maximum"))
        {
            final String[] pair = values.split(",");

            double left = Parser.eval(pair[0]);
            double right = Parser.eval(pair[1]);
            return Parser.integerizeWholeNumber(Math.max(left, right));
        }
        else if (findCaselessMatch(text, "@signed"))
        {
            if (Parser.isNumeric(values))
            {
                String left = Parser.evalAsTextWithLeadingPlusSupport(values);
                return left;
            }
            //FIXME one of these is correct...
            //return "";
            //return "+0";
            return "0";
        }
        else if (findCaselessMatch(text, "@signum"))
        {
            double left = Parser.eval(values);
            return Parser.integerizeWholeNumber(Math.signum(left));
        }
        else if (findCaselessMatch(text, "@abs"))
        {
            double left = Parser.eval(values);
            return Parser.integerizeWholeNumber(Math.abs(left));
        }
        else if (findCaselessMatch(text, "@ceil"))
        {
            double left = Parser.eval(values);
            return Parser.integerizeWholeNumber(Math.ceil(left));
        }
        else if (findCaselessMatch(text, "@floor"))
        {
            double left = Parser.eval(values);
            return Parser.integerizeWholeNumber(Math.floor(left));
        }
        else if (findCaselessMatch(text, "@log10"))
        {
            double left = Parser.eval(values);
            return Parser.integerizeWholeNumber(Math.log10(left));
        }
        else if (findCaselessMatch(text, "@nlog"))
        {
            double left = Parser.eval(values);
            return Parser.integerizeWholeNumber(Math.log(left));
        }
        else if (findCaselessMatch(text, "@sqrt"))
        {
            double left = Parser.eval(values);
            return Parser.integerizeWholeNumber(Math.sqrt(left));
        }
        else if (findCaselessMatch(text, "@InchesToFeet"))
        {
            double left = Parser.eval(values);
            int feet = (int) left / 12;
            int inches = ((int) left - (feet * 12));
            return feet + "' " + inches + "\"";
        }
        else if (findCaselessMatch(text, "@ifdefined"))
        {
            // this version adds a space to allow for word chaining
            if (values != null && values.trim().length() > 0)
            {
                return values + " ";
            }
            return "";
        }
        else if (findCaselessMatch(text, "@ifdefinedonly"))
        {
            // this method just plops it wherever you put it
            if (values != null && values.trim().length() > 0)
            {
            	return values;
            }
            return "";
        }
        else if (findCaselessMatch(text, "@ternary"))
        {
            if (values != null && values.trim().length() > 0)
            {
                String[] elements = values.split("\\|");
                String test = elements[0];
                if (Parser.isMostlyTrue(test)) {
                    return elements[1];
                }
                return elements[2];
                //convertVariables(gc, passed, ce, vars, result);
            }
            return "";
        }
        else if (findCaselessMatch(text, "@orelse"))
        {
            // this could be so much more!!

        	// this sends this processing to the 'end' - other code should remove this
        	// unless there is nothing before it on the line but spaces.
        	return "???" + values + "???";
        }
        else if (findCaselessMatch(text, "@list"))
        {
            console.info("could do more here - convert into vertical list or html list");
            if (values.startsWith("[") && values.endsWith("]")) {
                return values.substring(1, values.length()-1);
            }
            return values;
        }

        return "" + text;
    }

  public static boolean findCaselessMatch(final String text, final String lookup)
  {
      if (text.startsWith(lookup.toLowerCase()) || text.startsWith(lookup.toUpperCase()) || text.startsWith(Parser.capitalize(lookup)))
      {
          return true;
      }
      return false;
  }

    /**
     * @param text
     * @return text properly setup for a regex replace.
     */
    public static String preReplace(final String text)
    {
        return Pattern.quote(text);
    }

    /**
     * this is used to convert 'short form' variable lists into long form ones.
     * i.e.
     * a^b^c^d^e => ~a~~b~~c~~d~~e~
     *
     * @param text short form text
     * @return long form, variablized text
     */
    public static String expandVariables(String text)
    {
        String result = new String(text);
        if (result.contains("^"))
        {
            console.debug("expanding name generator style substitution text = '%s'", result);
            List<String> parts = new ArrayList<String>(Arrays.asList(result.split("\\|")));

            // these two steps are necessary to prevent occlusion of parts
            Collections.sort(parts);
            Collections.reverse(parts);

            console.debug("parts list:");
            for (String part : parts)
			{
                console.debug(" %s", part);

			}
            for (String part : parts)
			{
            	console.debug("  part='%s'", part);
                List<String> splits = new ArrayList<String>(Arrays.asList(part.split("[\\s.,!?~'\"]")));
                Set<String> set = new HashSet<String>(splits);
                List<String> list = new ArrayList<String>(set);
                Collections.sort(list);
                Collections.reverse(list);
            	// handle multi-selects
                String part1 = "";
                for (String e : list)
                {
                	console.debug("    e : '%s'", e);
                	part1 = (e.contains("^") ? expandVariablesOld(e): e);
                	// these can be generically replaced.
                	while(result.contains(e) && e.contains("^"))
                	{
                		result = Parser.findAndReplace(result, e, part1);
                		console.debug("    r : '%s'", result);
                	}
                }

			}
            console.debug("final expansion = '%s'", result);
        }
        return result;
    }

    /**
     * this is used to convert 'short form' variable lists into long form ones.
     * i.e.
     * a^b^c^d^e => ~a~~b~~c~~d~~e~
     *
     * @param text short form text
     * @return long form, variablized text
     */
    private static String expandVariablesOld(String text)
    {
        String result = String.copyValueOf(text.toCharArray());
        if (result.contains("^"))
        {
            console.debug("expanding name generator style substitution text = '%s'", result);
            List<String> splits = new ArrayList<String>(Arrays.asList(result.split("\\^|\\|")));
            Set<String> set = new HashSet<String>(splits);
            List<String> list = new ArrayList<String>(set);
            Collections.sort(list);
            Collections.reverse(list);
        	// handle multi-selects
            result = smartReplaceWorker(result, list, "^");
            result = result.replaceAll("\\^", "");
            console.debug("final expansion = '%s'", result);

        }
        return result;
    }

	private static String smartReplaceWorker(String result, List<String> listOfElements, String delimiters)
	{
		final StringBuilder sb = new StringBuilder();

		int startPointer = 0;
		int endPointer = 1;
		String currentElement = "";
		while(startPointer <= result.length() && endPointer <= result.length())
		{
			if (endPointer < result.length() && delimiters.contains(result.substring(endPointer, endPointer + 1)))
			{
				// process current string
				currentElement = result.substring(startPointer, endPointer);
				if (listOfElements.contains(currentElement))
				{
					console.debug("found element = %s", currentElement);
					sb.append("~" + currentElement + "~");
				}
				sb.append(result.substring(endPointer, endPointer+1));
				startPointer = endPointer+1;
				endPointer +=2;
				if (endPointer > result.length())
				{
					break;
				}
			}
			else
			{
				currentElement = result.substring(startPointer, endPointer++);
			}

		}
		//hit the end, add final element.
		sb.append("~" + currentElement + "~");
		console.debug("final result = %s", sb.toString());
		return sb.toString();
	}

	public static void main(String[] args)
	{
		final String check = "SVC^VV^ECV Al SVC^VC|SCV^CV Saud SVC^VC|SVC^VV^ECV Pash SVC^VC|SVC^VC Bin SVC^VC|SVC^VC Al SVC^VC|SCV^CV|SCV^CV Pash SCV^EVC|SVC^VV^ECV|SVC^VC|SVC^VC";
		//final String check = "SVC^VV^ECV|SVC^VC|SVC^VC";
		expandVariables(check);
	}

    public static String raise(String var) {
        return String.format("%s%s%s", Constants.VARIABLE_DELIMITER, var, Constants.VARIABLE_DELIMITER);
    }

    public static String deraise(String var) {
        if (var.startsWith("~") && var.endsWith("~")) {
            return var.substring(1, var.length() - 1);
        }
        return var;
    }

    /**
     * this is specifically for text generation
     * takes two items strips out the numbers, adds them together than creates a new string with the new number
     */
    public static String addTwoItems(String item1, String item2) {
        console.warn("need to account for ~1dX~ type constructs and already 'manyed' items");
        Integer first = Parser.findFirstNumber(item1);
        Integer second = Parser.findFirstNumber(item2);
        if (first == null) first = 0;
        if (second == null) second = 0;
        int result = (first + second);
        if (result == 0) result = 2;
        String value = result + " " + (first == 0 ? item1 : Parser.afterParse(item1, " "));
        console.info("created '%s' from %s / %s", value, item1, item2);
        return value;
    }
}
