package org.refracktory.refrackt.util;

public class Constants
{
    public static final String SELECTOR_ITEM_DELIMITER = "◄";
    public static final String SELECTOR_ITEM_SPLIT_DELIMITER = "◄";
    public static final String CR = "\r";

    public static final char EMPTY_VARIABLE_DELIMITER = '`';
    public static final char VARIABLE_DELIMITER = '~';
    public static final String END_OF_LINE_CHARACTER = System.getProperty( "line.separator" );
    public static final String RIGHT_CONSTRAINT_DELIMITER 	= "}";
    public static final String LEFT_CONSTRAINT_DELIMITER 	= "{";
    public static final char TAB_CHARACTER = '†';

    public static final String NOT_SUBSTITUTED = "≈";
    public static final char NOT_SUBSTITUTED_C = '≈';
    public static final int COMMENT_LINE = 66;
    public static final int END_OF_LINE = 13;

}
