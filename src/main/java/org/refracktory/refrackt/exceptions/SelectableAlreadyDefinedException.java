package org.refracktory.refrackt.exceptions;

public class SelectableAlreadyDefinedException extends Exception {
  public SelectableAlreadyDefinedException(String message) {
    super(message);
  }
}
