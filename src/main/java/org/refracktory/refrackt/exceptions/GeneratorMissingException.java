package org.refracktory.refrackt.exceptions;

public class GeneratorMissingException extends Exception {
    public GeneratorMissingException(String message) {
        super(message);
    }
}
