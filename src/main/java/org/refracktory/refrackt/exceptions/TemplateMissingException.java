package org.refracktory.refrackt.exceptions;

public class TemplateMissingException extends Exception {
    public TemplateMissingException(String message) {
        super(message);
    }
}
