package org.refracktory.refrackt.exceptions;

public class DataTableAlreadyDefinedException extends Exception {
    public DataTableAlreadyDefinedException(String text) {
        super(text);
    }
}
