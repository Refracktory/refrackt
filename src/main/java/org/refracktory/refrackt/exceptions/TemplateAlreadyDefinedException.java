package org.refracktory.refrackt.exceptions;

public class TemplateAlreadyDefinedException extends Exception {
    public TemplateAlreadyDefinedException(String message) {
        super(message);
    }
}
