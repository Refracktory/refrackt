package org.refracktory.refrackt.exceptions;

public class DataTableMissingException extends Throwable {
    public DataTableMissingException(String text) {
        super(text);
    }
}
