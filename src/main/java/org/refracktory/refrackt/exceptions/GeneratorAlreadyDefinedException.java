package org.refracktory.refrackt.exceptions;

public class GeneratorAlreadyDefinedException extends Exception {
    public GeneratorAlreadyDefinedException(String message) {
        super(message);
    }
}
