package org.refracktory.refrackt.exceptions;

public class SelectableMissingException extends Exception {
  public SelectableMissingException(String message) {
    super(message);
  }
}
